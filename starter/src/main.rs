#![allow(dead_code)]
#![allow(unused_variables)]
#![allow(unused_mut)]
#![allow(unused_imports)]

extern crate rand;

mod commander01;
mod macros;
mod mygame;

use commander01::MyCommander;
use mygame::*;

macro_rules! parse_input {
    ($x:expr, $t:ident) => {
        $x.trim().parse::<$t>().unwrap()
    };
}

macro_rules! read_line {
    ($t:ident) => {{
        use std::io;
        use std::io::Write;

        $t.clear();
        io::stdin().read_line(&mut $t).unwrap();
        //print_err!("{}", &$t);
        write!(&mut ::std::io::stderr(), "{}", $t).ok();
    }};
}

// ===============================================================================================

fn main() {
    let mut input_line = String::new();
    // read_line!(input_line);

    let mut commander = MyCommander::new();
    // game loop
    let mut round = 0;
    let nb_players = 2;
    loop {
	round += 1;
        let unit_count = parse_input!(input_line, i32);
        let entities = (0..unit_count)
            .map(|i| {
                read_line!(input_line);
                Entity::from(&input_line)
            })
            .collect::<Vec<Entity>>();
        // Write an action using println!("message...");
        // To debug: print_err!("Debug message...");
        let states = States {
            round: round,
            // scores: scores,
            entities: entities,
        };
        print_debug!("{:?}", &states);
        if round == 0 {
            commander.init(&states);
        }
        for a in commander.actions(&states) {
            println!("{}", a);
        }
        // round of other player
        round += nb_players - 1;
    }
}
