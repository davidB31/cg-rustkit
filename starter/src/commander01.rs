use mygame::*;

pub struct MyCommander;
impl MyCommander {
    pub fn new() -> Self {
        MyCommander {}
    }
}
impl Commander for MyCommander {
    fn init(&mut self, states: &States) {}
    fn actions(&mut self, states: &States) -> Vec<Action> {
        vec![Action::NoAction]
    }
}
