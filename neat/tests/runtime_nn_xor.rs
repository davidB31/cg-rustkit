extern crate neat;
#[macro_use]
extern crate spectral;

use neat::runtime::*;
use spectral::prelude::*;

#[test]
fn test_hardcoded_xor() {
    let mut sut = NeuralNetExecutor::from(make_nn_xor());
    // println!("{:?}", sut);
    // bias value set at index 0
    assert_that!(&sut.update(&vec![0f64, 0f64])[0]).is_close_to(0f64, 0.01f64);
    assert_that!(&sut.update(&vec![1f64, 1f64])[0]).is_close_to(0f64, 0.01f64);

    assert_that!(&sut.update(&vec![0f64, 1f64])[0]).is_close_to(1f64, 0.01f64);
    assert_that!(&sut.update(&vec![1f64, 0f64])[0]).is_close_to(1f64, 0.01f64);
}

fn make_nn_xor() -> NeuralNetGraphDef {
    let inputs_count = 2;
    let outputs_count = 1;
    let links = vec![
        //bias at 0
        Link {
            src: 0,
            dst: 4,
            weight: -10f64,
        },
        Link {
            src: 0,
            dst: 5,
            weight: 30f64,
        },
        Link {
            src: 0,
            dst: 3,
            weight: -30f64,
        },
        //
        Link {
            src: 1,
            dst: 4,
            weight: 20f64,
        },
        Link {
            src: 1,
            dst: 5,
            weight: -20f64,
        },
        Link {
            src: 2,
            dst: 4,
            weight: 20f64,
        },
        Link {
            src: 2,
            dst: 5,
            weight: -20f64,
        },
        Link {
            src: 4,
            dst: 3,
            weight: 20f64,
        },
        Link {
            src: 5,
            dst: 3,
            weight: 20f64,
        },
    ];
    NeuralNetGraphDef {
        silhouette: NeuralNetSilhouette {
            inputs_count,
            outputs_count,
        },
        links,
        ..Default::default()
    }
}
