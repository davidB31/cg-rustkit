extern crate neat;
#[macro_use]
extern crate spectral;
extern crate rand;
extern crate rayon;

use neat::runtime::*;
use neat::training::GeneticCfg;
use neat::training::MutationCfg;
use neat::training::{EvolutionRun, Genome};
use rand::distributions::Uniform;
use rand::distributions::Weighted;
use rayon::prelude::*;
use spectral::prelude::*;

#[test]
fn test_training_xor() {
    let mut sut = NeuralNetExecutor::from(make_nn_xor());
    // println!("{:?}", sut);
    // bias value set at index 0
    assert_that!(&sut.update(&vec![0f64, 0f64])[0]).is_close_to(0f64, 0.01f64);
    assert_that!(&sut.update(&vec![1f64, 1f64])[0]).is_close_to(0f64, 0.01f64);

    assert_that!(&sut.update(&vec![0f64, 1f64])[0]).is_close_to(1f64, 0.01f64);
    assert_that!(&sut.update(&vec![1f64, 0f64])[0]).is_close_to(1f64, 0.01f64);
}

fn dump_graph<S: Into<String>>(nn_def: &NeuralNetGraphDef, fpath: S) {
    use neat::exporters::to_d3_json;
    use std::fs;

    fs::write(
        fpath.into(),
        to_d3_json(nn_def).expect("can convert to json str"),
    );
}

fn make_nn_xor() -> NeuralNetGraphDef {
    let cfg = GeneticCfg {
        population_size: 150,
        silhouette: NeuralNetSilhouette {
            inputs_count: 2,
            outputs_count: 1,
        },
        mutation_cfg: MutationCfg {
            weights_range: Uniform::new_inclusive(-20f64, 20f64),
            ..Default::default()
        },
        coeff_matched: 0.4f64,  // to increase with population size
        dist_threshold: 3.0f64, // to increase with population size & weights_range
        ..Default::default()
    };
    let mut runner = EvolutionRun::new(cfg.clone());
    let mut rng = rand::thread_rng();
    // empty vec![] to have a random genome;
    //let mut last_experiment_cfg = vec![];
    // start with fully linked
    let g0 = runner.new_fully_linked_inout(&mut rng);
    let mut last_experiment_cfg = vec![
        Weighted {
            weight: 0,
            item: g0
        };
        cfg.population_size
    ];
    let mut prev_best_score = 0;
    for generation in 0..100 {
        let exps = next(&mut runner, &mut rng, last_experiment_cfg);
        assert_that!(exps.len()).is_equal_to(cfg.population_size);
        last_experiment_cfg = exps.par_iter()
            .cloned()
            .map(|exp| Weighted {
                weight: score_of(&exp),
                item: exp,
            })
            .collect::<Vec<_>>();
        let best = last_experiment_cfg.iter().max_by_key(|w| w.weight).unwrap();
        let worst = last_experiment_cfg.iter().min_by_key(|w| w.weight).unwrap();
        eprintln!(
            "next epoch: generarion: {:?} scores: [{:?},{:?}] inno ({:?}, {:?})",
            generation,
            worst.weight,
            best.weight,
            &runner.innovation_repo.last_innovation_idx(),
            &runner.innovation_repo.last_neuron_id()
        );
        dump_graph(
            &NeuralNetGraphDef::from(&best.item),
            format!("tools/data/graph_{}.json", generation),
        );
        //eprintln!("innovation_repo: {:?}", &runner.innovation_repo);
        //assert_that!(best.weight).is_greater_than_or_equal_to(prev_best_score);
        prev_best_score = best.weight;
        /*if best_score > 9800 {
            break;
        }*/
    }
    let selection = last_experiment_cfg
        .iter()
        .max_by_key(|w| w.weight)
        .expect("to have one selection");
    (&selection.item).into()
}

fn score_of(genome: &Genome) -> u32 {
    let nn_def = NeuralNetGraphDef::from(genome);
    let mut sut = NeuralNetExecutor::from(nn_def);
    let err: f64 = vec![
        (vec![0f64, 0f64], 0f64),
        (vec![1f64, 0f64], 1f64),
        (vec![0f64, 1f64], 1f64),
        (vec![1f64, 1f64], 0f64),
    ].iter()
        .map(|(input, expected_output)| {
            let res = sut.update(input)[0];
            assert!(0f64 <= res && res <= 1f64, format!("0 <= {:?} <= 1", res));
            let diff = (res - expected_output);
            // .abs()
            // .max(0f64)
            // .min(1f64);
            // if diff > 0.99f64 {
            //     eprintln!("{:?}", diff);
            // }
            //let v = 1f64 - diff;
            diff.abs() // should be positive
        })
        //.fold(1f64, |acc, v| acc * v);
    .sum();
    //((4.0f64 - err) * 10000f64) as u32
    ((4f64 - err).powf(2f64) * 1000f64) as u32
}

fn next<R>(runner: &mut EvolutionRun, rng: &mut R, parent: Vec<Weighted<Genome>>) -> Vec<Genome>
where
    R: rand::Rng,
{
    runner.epoch(parent, rng)
}
