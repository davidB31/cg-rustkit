# NEAT (NeuroEvolution of Augmenting Topologies)

It's a try to implement [NEAT](http://www.cs.ucf.edu/~kstanley/neat.html). It's based on :

* the original paper [Efficient Evolution of Neural Network Topologies](http://nn.cs.utexas.edu/downloads/papers/stanley.cec02.pdf) by Kenneth O. Stanley and Risto Miikkulainen
* the implementation provided in the book [AI Techniques for Game Programming](http://www.amazon.com/exec/obidos/ASIN/193184108X/qid%3D1034633319/sr%3D1/1-1/ref%3Dsr_11_1/104-5338435-0523903)
* the doc in [neat python](http://neat-python.readthedocs.io/en/latest/neat_overview.html)
* the tutorial [Tutorial – Evolving Neural Networks with SharpNEAT 2](http://www.nashcoding.com/2010/07/17/tutorial-evolving-neural-networks-with-sharpneat-2-part-1/) (3 parts)

## Constraints

Generate a NN (neural network) usable into a Multi-bot competition on [CodinGame](http:www.codingame.com) :

* runtime:
  * no library dependency
  * the NN shoult be small enought to fit the size limitation
  * the process'inputs could be translated into NN's inputs
  * the NN'outputs could be translated into process' output
* the learning / training :
  * run locally (with cg-brutaltester or equivalent)
  * each run, could be a single process execution
  * no restriction about libraries (vs runtime)

## Links

* severals [implementations of NEAT/HyperNEAT/Novelty Search](http://eplex.cs.ucf.edu/neat_software/)
* an other Rust implementation : [rustneat](https://crates.io/crates/rustneat)

## Usage

### For CodinGame

1. copy / paste runtime/*.rs into the src/neat/runtime of dest project (vendoring the runtime)
2. copy / paste the output of

```rust
format!("let nn = {:?};", nn).replace(" [", " vec![");
```
