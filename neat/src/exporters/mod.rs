use runtime::find_max_of_idx;
use runtime::NeuralNetGraphDef;
use serde_json;

#[derive(Debug, Deserialize, Serialize, Clone, PartialEq)]
#[serde(rename_all = "camelCase")]
struct Graph {
    nodes: Vec<Node>,
    edges: Vec<Edge>,
}
#[derive(Debug, Deserialize, Serialize, Clone, PartialEq)]
#[serde(rename_all = "camelCase")]
enum Group {
    Bias,
    Input,
    Output,
    Hidden,
}

#[derive(Debug, Deserialize, Serialize, Clone, PartialEq)]
#[serde(rename_all = "camelCase")]
struct Node {
    id: usize,
    group: Group,
}

#[derive(Debug, Deserialize, Serialize, Clone, PartialEq)]
#[serde(rename_all = "camelCase")]
struct Edge {
    source: usize,
    target: usize,
    weight: f64,
}

//#[cfg(exporter)]
impl<'a> From<&'a NeuralNetGraphDef> for Graph {
    fn from(nn_def: &NeuralNetGraphDef) -> Self {
        let nodes = (0..=find_max_of_idx(&nn_def.links, nn_def.silhouette.hidden_idx_min() - 1))
            .map(|i| {
                let group = if nn_def.silhouette.is_bias(i) {
                    Group::Bias
                } else if nn_def.silhouette.is_input(i) {
                    Group::Input
                } else if nn_def.silhouette.is_output(i) {
                    Group::Output
                } else {
                    Group::Hidden
                };
                Node { id: i, group }
            })
            .collect();
        let edges = nn_def
            .links
            .iter()
            .map(|v| Edge {
                source: v.src,
                target: v.dst,
                weight: v.weight,
            })
            .collect();
        Graph { nodes, edges }
    }
}

pub fn to_d3_json(nn_def: &NeuralNetGraphDef) -> Result<String, serde_json::Error> {
    serde_json::to_string(&Graph::from(nn_def))
}

#[cfg(test)]
mod tests {
    use super::*;
    use runtime::{Link, NeuralNetSilhouette};
    use spectral::prelude::*;

    // let v = NeuralNetGraphDef {
    //     silhouette: NeuralNetSilhouette {
    //         inputs_count: 2,
    //         outputs_count: 1,
    //     },
    //     links: vec![
    //         Link {
    //             src: 0,
    //             dst: 3,
    //             weight: 0.1,
    //         },
    //         Link {
    //             src: 1,
    //             dst: 3,
    //             weight: 0.2,
    //         },
    //         Link {
    //             src: 2,
    //             dst: 3,
    //             weight: 0.3,
    //         },
    //     ],
    //     ..Default::default()
    // };
    fn normalize_json_graph(str: &str) -> String {
        let g: Graph = serde_json::from_str(str).unwrap();
        serde_json::to_string(&g).unwrap()
    }

    #[test]
    fn test_to_d3_json_empty() {
        let expected =
            normalize_json_graph(r#"{"nodes":[{"id": 0, "group": "bias"}], "edges": []}"#);
        let actual = NeuralNetGraphDef {
            ..Default::default()
        };
        assert_that!(&to_d3_json(&actual).unwrap()).is_equal_to(&expected);
    }

    #[test]
    fn test_to_d3_json_simple_2_1() {
        let expected = normalize_json_graph(r#"{"nodes":[{"id": 0, "group":"bias"}, {"id": 1, "group": "input"}, {"id": 2, "group": "output"}], "edges": [{"source": 1, "target": 2, "weight": 33}]}"#);
        let actual = NeuralNetGraphDef {
            silhouette: NeuralNetSilhouette {
                inputs_count: 1,
                outputs_count: 1,
            },
            links: vec![Link {
                src: 1,
                dst: 2,
                weight: 33f64,
            }],
            ..Default::default()
        };
        assert_that!(&to_d3_json(&actual).unwrap()).is_equal_to(&expected);
    }
}
