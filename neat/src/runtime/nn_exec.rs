use super::*;

/// runtime executor of a Neural network (created from NeuralNetGraphDef)
#[derive(Debug, Clone, PartialEq)]
pub struct NeuralNetExecutor {
    nn_def: NeuralNetGraphDef,
    max_of_id: NeuronIdx,
    links_by_dst: Vec<Vec<Link>>,
    computing_order: Vec<NeuronIdx>,
}

impl From<NeuralNetGraphDef> for NeuralNetExecutor {
    fn from(v: NeuralNetGraphDef) -> Self {
        let max_of_id = find_max_of_idx(&v.links, v.silhouette.hidden_idx_min() - 1);
        let links_by_dst = group_links_by_dst(max_of_id, &v.links);
        let computing_order = find_forward_computing_order(&v, &links_by_dst);
        NeuralNetExecutor {
            nn_def: v,
            max_of_id,
            links_by_dst,
            computing_order,
        }
    }
}

impl NeuralNetExecutor {
    /// update network for this clock cycle
    /// inputs - all the inputs should be scaled to the interval 0 to 1.
    /// ouputs - a Vec of values in the interval 0 to 1.
    //TODO manage recurrent node (how ?)
    pub fn update(&mut self, inputs: &[f64]) -> Vec<f64> {
        let mut neurons_values = vec![0f64; self.max_of_id + 1];
        //TODO replace assert by dependent type (size) or an unpanic error
        assert_eq!(
            inputs.len(),
            self.nn_def.silhouette.inputs_count,
            "expected {:?} inputs values, received: {:?}",
            self.nn_def.silhouette.inputs_count,
            inputs.len()
        );
        // set inputs
        neurons_values[self.nn_def.silhouette.bias_idx()] = 1.0;
        (0..inputs.len())
            .for_each(|n| neurons_values[self.nn_def.silhouette.inputs_idx_min() + n] = inputs[n]);
        // compute hidden and output
        self.computing_order
            .iter()
            .skip(self.nn_def.silhouette.inputs_idx_max() + 1)
            .for_each(|n| {
                //eprintln!("n: {:?} neurons_values: {:?}", n, neurons_values);
                let sum = self.links_by_dst[*n]
                    .iter()
                    .map(|link| neurons_values[link.src] * link.weight)
                    .sum();
                //now put the sum through the activation function and assign the
                //value to this neuron's output
                let response = if self.nn_def.activations_responses.is_empty() {
                    1f64
                } else {
                    self.nn_def.activations_responses[*n]
                };
                neurons_values[*n] = sigmoid(sum, response);
            });
        //eprintln!("neurons_values: {:?}", neurons_values);
        neurons_values
            [self.nn_def.silhouette.outputs_idx_min()..=self.nn_def.silhouette.outputs_idx_max()]
            .to_vec()
    }
}

//TODO benchmarck vs lookup table
fn sigmoid(activation: f64, response: f64) -> f64 {
    //1f64 / (1f64 + (-y).exp())
    1f64 / (1f64 + (-activation / response).exp())
}

#[cfg(test)]
mod tests {
    use super::*;
    use spectral::prelude::*;

    #[test]
    fn test_sigmoid() {
        assert_that!(&sigmoid(0.0f64, 1.0f64)).is_close_to(0.50f64, 0.01f64);
        assert_that!(&sigmoid(0.5f64, 1.0f64)).is_close_to(0.62f64, 0.01f64);
        assert_that!(&sigmoid(1.0f64, 1.0f64)).is_close_to(0.73f64, 0.01f64);
        assert_that!(&sigmoid(-1.0f64, 1.0f64)).is_close_to(0.26f64, 0.01f64);
    }

    fn simplest_nn() -> NeuralNetGraphDef {
        NeuralNetGraphDef {
            silhouette: NeuralNetSilhouette {
                inputs_count: 1,
                outputs_count: 1,
            },
            links: vec![Link {
                src: 1,
                dst: 2,
                weight: 1f64,
            }],
            ..Default::default()
        }
    }

    #[test]
    fn test_the_simplest_nn_1_to_1() {
        let mut sut = NeuralNetExecutor::from(simplest_nn());
        assert_that!(&sut.update(&vec![0.0f64]).len()).is_equal_to(1);
        assert_that!(&sut.update(&vec![0.0f64])[0]).is_close_to(0.50f64, 0.01f64);
        assert_that!(&sut.update(&vec![0.5f64])[0]).is_close_to(0.62f64, 0.01f64);
        assert_that!(&sut.update(&vec![1.0f64])[0]).is_close_to(0.73f64, 0.01f64);
    }

    #[test]
    fn test_nn_with_unlinked_node() {
        let nn = NeuralNetGraphDef {
            silhouette: NeuralNetSilhouette {
                inputs_count: 2,
                outputs_count: 1,
            },
            links: vec![],
            ..Default::default()
        };
        let mut sut = NeuralNetExecutor::from(nn);
        assert_that!(&sut.update(&vec![1.0f64, 1.0f64]).len()).is_equal_to(1);
        assert_that!(&sut.update(&vec![1.0f64, 1.0f64])[0]).is_close_to(0.5f64, 0.01f64); // sigmoid(0)
    }

    #[test]
    fn test_dump_read_write() {
        let sut = simplest_nn();
        let str = format!("let neuralnet = {:?};", sut).replace(" [", " vec![");
        //eprintln!("{}", str);
        // expected is what is print on the stderr and that could be copy/pasted
        let expected = r#"let neuralnet = NeuralNetGraphDef { silhouette: NeuralNetSilhouette { inputs_count: 1, outputs_count: 1 }, links: vec![Link { src: 1, dst: 2, weight: 1.0 }], activations_responses: vec![] };"#;
        assert_that!(&str).is_equal_to(expected.to_string());
    }

}
