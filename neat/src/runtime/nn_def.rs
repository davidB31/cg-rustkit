use super::NeuralNetSilhouette;
use super::NeuronIdx;

/// NeuralNetGraphDef is the minimal data required to generate a computing
/// graph (for neural network) Each computing node (output + hidden) compute
/// output value by:
/// - sigmoid(sum(input.links(weight * src.value)))
/// - sigmoid always use activation_response = 1.0

#[derive(Debug, Clone, PartialEq, Default)]
pub struct NeuralNetGraphDef {
    pub silhouette: NeuralNetSilhouette,
    pub links: Vec<Link>,
    /// activations of every neuron
    pub activations_responses: Vec<f64>,
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Link {
    pub src: NeuronIdx,
    pub dst: NeuronIdx,
    pub weight: f64,
}

pub fn find_max_of_idx(links: &Vec<Link>, min: NeuronIdx) -> NeuronIdx {
    links.iter().fold(min, |acc, v| acc.max(v.dst).max(v.src))
}

/// convert a list of links into a table of list of links where
/// index is the NeuronIdx of 'dst'.
/// return a vector of links group by 'dst' field of the link.
pub fn group_links_by_dst(max_of_idx: NeuronIdx, links: &Vec<Link>) -> Vec<Vec<Link>> {
    links
        .iter()
        .fold(vec![vec![]; max_of_idx + 1], |mut acc, v| {
            acc[v.dst].push(v.clone());
            acc
        })
}

/// compute the order to process neuron to do forward computing
/// produce an array composed of 3 parts: [inputs, hiddens, outputs]
/// TODO insert inputs, outputs in reverse order
pub fn find_forward_computing_order(
    nn_def: &NeuralNetGraphDef,
    links_by_dst: &Vec<Vec<Link>>,
) -> Vec<NeuronIdx> {
    let mut out = vec![];
    // push every outputs to be computed at the end
    (nn_def.silhouette.outputs_idx_min()..=nn_def.silhouette.outputs_idx_max())
        .for_each(|n| out.push(n));
    // go backward to push hidden neuron
    let ignore_below = nn_def.silhouette.hidden_idx_min() - 1;
    (nn_def.silhouette.outputs_idx_min()..=nn_def.silhouette.outputs_idx_max())
        .for_each(|n| find_backward(n, &ignore_below, links_by_dst, &mut out));
    // push every inputs to be computed at the beginning
    (nn_def.silhouette.inputs_idx_min()..=nn_def.silhouette.inputs_idx_max())
        .for_each(|n| out.push(n));
    out.push(nn_def.silhouette.bias_idx());
    out.reverse();
    out
}

//TODO optimize the dedup by using set
fn find_backward(
    dst: NeuronIdx,
    ignore_below: &NeuronIdx,
    links_by_dst: &Vec<Vec<Link>>,
    out: &mut Vec<NeuronIdx>,
) {
    let srcs = links_by_dst[dst]
        .iter()
        .map(|l| l.src)
        .filter(|i| i > ignore_below)
        .collect::<Vec<_>>();
    // first add every first level
    for n in &srcs {
        if !out.contains(&n) {
            out.push(n.clone());
        }
    }
    // first add every deep level
    for n in srcs {
        find_backward(n, ignore_below, links_by_dst, out);
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use spectral::prelude::*;

    #[test]
    fn test_find_forward_computing_order_no_links() {
        let v = NeuralNetGraphDef {
            silhouette: NeuralNetSilhouette {
                inputs_count: 2,
                outputs_count: 1,
            },
            ..Default::default()
        };
        let max_of_id = find_max_of_idx(&v.links, v.silhouette.hidden_idx_min() - 1);
        assert_that!(max_of_id).is_equal_to(3);
        let links_by_dst = group_links_by_dst(max_of_id, &v.links);
        let computing_order = find_forward_computing_order(&v, &links_by_dst);
        assert_that!(computing_order).is_equal_to(vec![0, 2, 1, 3]);
    }

    #[test]
    fn test_find_forward_computing_order_fully_links_no_hidden() {
        let v = NeuralNetGraphDef {
            silhouette: NeuralNetSilhouette {
                inputs_count: 2,
                outputs_count: 1,
            },
            links: vec![
                Link {
                    src: 0,
                    dst: 3,
                    weight: 0.1,
                },
                Link {
                    src: 1,
                    dst: 3,
                    weight: 0.2,
                },
                Link {
                    src: 2,
                    dst: 3,
                    weight: 0.3,
                },
            ],
            ..Default::default()
        };
        let max_of_id = find_max_of_idx(&v.links, v.silhouette.hidden_idx_min() - 1);
        assert_that!(max_of_id).is_equal_to(3);
        let links_by_dst = group_links_by_dst(max_of_id, &v.links);
        let computing_order = find_forward_computing_order(&v, &links_by_dst);
        assert_that!(computing_order).is_equal_to(vec![0, 2, 1, 3]);
    }

    #[test]
    fn test_find_forward_computing_order_with_hidden() {
        let v = NeuralNetGraphDef {
            silhouette: NeuralNetSilhouette {
                inputs_count: 2,
                outputs_count: 1,
            },
            links: vec![
                Link {
                    src: 0,
                    dst: 4,
                    weight: 0.1,
                },
                Link {
                    src: 1,
                    dst: 4,
                    weight: 0.2,
                },
                Link {
                    src: 4,
                    dst: 3,
                    weight: 1.0,
                },
                Link {
                    src: 2,
                    dst: 3,
                    weight: 0.3,
                },
            ],
            ..Default::default()
        };
        let max_of_id = find_max_of_idx(&v.links, v.silhouette.hidden_idx_min() - 1);
        assert_that!(max_of_id).is_equal_to(4);
        let links_by_dst = group_links_by_dst(max_of_id, &v.links);
        let computing_order = find_forward_computing_order(&v, &links_by_dst);
        assert_that!(computing_order).is_equal_to(vec![0, 2, 1, 4, 3]);
    }
}
