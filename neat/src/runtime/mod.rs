mod nn_def;
mod nn_exec;
mod nn_silhouette;

//pub use self::nn::*;
pub use self::nn_def::*;
pub use self::nn_exec::*;
pub use self::nn_silhouette::*;

pub type NeuronIdx = usize;
