use super::NeuronIdx;

/// - if bias needed then :
///     - use input node with index 0 (NeuronIdx = 0)
///     - create link src index 0 to node with bias
///     - set the weight of the link to bias value
///     - set its input value to 1.0
/// NeuronIdx (index of the (in the graph and links) are defined in the
/// following ranges:
/// - [0]: bias node
/// - [1,inputs_count]: input nodes (neurons)
/// - [inputs_count+1, inputs_count+outputs_count]: output nodes (neurons)
/// - [inputs_count+outputs_count+1,...]: hidden neurons (try to be not sparce)
#[derive(Debug, Clone, Copy, Eq, PartialEq, Default, Hash)]
pub struct NeuralNetSilhouette {
    pub inputs_count: usize,
    pub outputs_count: usize,
}

impl NeuralNetSilhouette {
    pub fn is_bias(&self, neuron_id: NeuronIdx) -> bool {
        neuron_id == 0
    }

    pub fn is_input(&self, neuron_id: NeuronIdx) -> bool {
        self.inputs_idx_min() <= neuron_id && neuron_id <= self.inputs_idx_max()
    }

    pub fn is_output(&self, neuron_id: NeuronIdx) -> bool {
        self.outputs_idx_min() <= neuron_id && neuron_id <= self.outputs_idx_max()
    }

    pub fn is_hidden(&self, neuron_id: NeuronIdx) -> bool {
        neuron_id >= self.hidden_idx_min()
    }

    pub fn bias_idx(&self) -> NeuronIdx {
        0
    }

    pub fn inputs_idx_min(&self) -> NeuronIdx {
        1
    }

    pub fn inputs_idx_max(&self) -> NeuronIdx {
        self.inputs_count
    }

    pub fn outputs_idx_min(&self) -> NeuronIdx {
        self.inputs_count + 1
    }

    pub fn outputs_idx_max(&self) -> NeuronIdx {
        self.inputs_count + self.outputs_count
    }

    pub fn hidden_idx_min(&self) -> NeuronIdx {
        self.inputs_count + self.outputs_count + 1
    }
}
