use super::Genome;
use rand::distributions::Weighted;

/// compare a target genome to a reference genome and
/// returns (count_disjoint, count_excess, count_matched,
/// sum_weight_differences_of_matching_genes)
fn count_diff(g1: &Genome, g2: &Genome) -> (usize, usize, usize, f64) {
    let mut count_disjoint = 0;
    let mut count_excess = 0;
    let mut count_matched = 0;
    let mut sum_weight_diff = 0f64;
    let mut g1_idx = 0;
    let mut g2_idx = 0;
    while (g1_idx < g1.count_genes()) && (g2_idx < g2.count_genes()) {
        if g1_idx == g1.count_genes() {
            g2_idx += 1;
            count_excess += 1;
        } else if g2_idx == g2.count_genes() {
            g1_idx += 1;
            count_excess += 1;
        } else {
            let g1_l = g1.find_gene_by_idx(g1_idx).expect("g1_idx valid");
            let g2_l = g2.find_gene_by_idx(g2_idx).expect("g2_idx valid");
            if g1_l.innovation_idx() == g2_l.innovation_idx() {
                g1_idx += 1;
                g2_idx += 1;
                count_matched += 1;
                sum_weight_diff += (g1_l.weight - g2_l.weight).abs();
            } else if g1_l.innovation_idx() > g2_l.innovation_idx() {
                g2_idx += 1;
                count_disjoint += 1;
            } else {
                g1_idx += 1;
                count_disjoint += 1;
            }
        }
    }
    (count_disjoint, count_excess, count_matched, sum_weight_diff)
}

// coeff_disjoint = 1f64
// coeff_excess = 1f64
// coeff_matched = 1f64
pub fn dist(
    g2: &Genome,
    g1: &Genome,
    coeff_disjoint: f64,
    coeff_excess: f64,
    coeff_matched: f64,
) -> f64 {
    let (count_disjoint, count_excess, count_matched, sum_weight_diff) = count_diff(g1, g2);
    let longest = g1.count_genes().max(g2.count_genes()) as f64;
    let avg_weight_diff = if count_matched > 0 {
        coeff_matched * sum_weight_diff / (count_matched as f64)
    } else {
        0f64
    };
    ((coeff_excess * (count_excess as f64)) / longest)
        + (coeff_disjoint * (count_disjoint as f64) / longest)
        + avg_weight_diff
}

#[derive(Debug, Clone, PartialEq)]
pub struct Specie {
    //pub id: SpecieId,
    pub leader: Genome,
    /* /// best fitness found so far by this species
     * // fitness_best: f64,
     * // fitness_avg: f64,
     * /// generations since fitness has improved, we can use
     * /// this info to kill off a species if required
     * gensNoImprovement: u32,
     */
    /// age of species
    born_at: u32,
    /* // how many of this species should be spawned for
     * // the next population
     * spawnsRqd: usize, */
}

// impl PartialEq for Specie {
//     fn eq(&self, other: &Self) -> bool {
//         self.id == other.id
//     }
// }

#[derive(Debug, Clone)]
pub struct Modifier {
    pub threshold: f64,
    pub value: f64,
}

impl Specie {
    pub fn fitness_coeff(
        &self,
        nb_entries: usize,
        bonus_young: &Modifier,
        malus_old: &Modifier,
        current_epoch: u32,
    ) -> f64 {
        let mut coeff = 1f64;
        let age = (current_epoch - self.born_at) as f64;
        if age <= bonus_young.threshold {
            coeff *= bonus_young.value
        } else if age >= malus_old.threshold {
            coeff *= malus_old.value
        }
        coeff / (nb_entries as f64)
    }

    fn select_new_leader(&mut self, populations: &Vec<Weighted<Genome>>) {
        self.leader = populations
            .iter()
            .max_by_key(|v| v.weight)
            .unwrap_or(&populations[0])
            .item
            .clone();
    }

    fn adjust_fitness(
        &self,
        populations: Vec<Weighted<Genome>>,
        bonus_young: &Modifier,
        malus_old: &Modifier,
        current_epoch: u32,
    ) -> Vec<Weighted<Genome>> {
        let coeff = self.fitness_coeff(populations.len(), bonus_young, malus_old, current_epoch);
        populations
            .into_iter()
            .map(|g| Weighted {
                weight: (g.weight as f64 * coeff) as u32,
                item: g.item,
            })
            .collect()
    }

    pub fn speciation(
        population: Vec<Weighted<Genome>>,
        species: Vec<Specie>,
        bonus_young: &Modifier,
        malus_old: &Modifier,
        coeff_disjoint: f64,
        coeff_excess: f64,
        coeff_matched: f64,
        dist_threshold: f64,
        current_epoch: u32,
    ) -> Vec<(Specie, Vec<Weighted<Genome>>)> {
        let mut next_species = species
            .into_iter()
            .map(|key| (key, vec![]))
            .collect::<Vec<_>>();
        for wg in population {
            if let Some(pos) = next_species.iter().position(|s| {
                let d = dist(
                    &s.0.leader,
                    &wg.item,
                    coeff_disjoint,
                    coeff_excess,
                    coeff_matched,
                );
                //eprintln!("dist: {:?} <= {:?}", d, dist_threshold);
                d <= dist_threshold
            }) {
                if let Some(v) = next_species.get_mut(pos) {
                    v.1.push(wg);
                }
            } else {
                let specie = Specie {
                    leader: wg.item.clone(),
                    born_at: current_epoch,
                };

                next_species.push((specie, vec![wg]));
            }
        }
        next_species.retain(|v| !v.1.is_empty());
        next_species
            .into_iter()
            .map(|s| {
                let mut specie = s.0;
                let wgs = specie.adjust_fitness(s.1, bonus_young, malus_old, current_epoch);
                specie.select_new_leader(&wgs);
                (specie, wgs)
            })
            .collect::<Vec<_>>()
    }
}
