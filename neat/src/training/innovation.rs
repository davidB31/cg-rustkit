use super::InnovationIdx;
use super::NeuronId;

/// new neuron inserted between neuron_src and neuron_dst
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct NewNeuron {
    pub neuron_id: NeuronId,
    neuron_src: NeuronId,
    neuron_dst: NeuronId,
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct NewLink {
    // chronological index of creation of the NewLink
    pub innovation_idx: InnovationIdx,
    pub neuron_src: NeuronId,
    pub neuron_dst: NeuronId,
}

impl NewLink {
    pub fn is_recurrent(&self) -> bool {
        self.neuron_dst == self.neuron_src
    }
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct InnovationRepo {
    next_innovation_idx: InnovationIdx,
    next_neuron_id: NeuronId,
    neurons: Vec<NewNeuron>,
    links: Vec<NewLink>,
}

impl InnovationRepo {
    pub fn new(inputs_count: usize, outputs_count: usize) -> Self {
        InnovationRepo {
            next_innovation_idx: 0,
            next_neuron_id: inputs_count + outputs_count + 1,
            neurons: vec![],
            links: vec![],
        }
    }

    pub fn last_innovation_idx(&self) -> InnovationIdx {
        self.next_innovation_idx - 1
    }

    pub fn last_neuron_id(&self) -> NeuronId {
        self.next_neuron_id - 1
    }

    pub fn find_link_by_idx(&self, innovation_idx: InnovationIdx) -> Option<&NewLink> {
        self.links
            .iter()
            .find(|l| l.innovation_idx == innovation_idx)
    }

    pub fn find_link(&self, neuron_src: NeuronId, neuron_dst: NeuronId) -> Option<&NewLink> {
        self.links
            .iter()
            .find(|i| i.neuron_src == neuron_src && i.neuron_dst == neuron_dst)
    }

    pub fn add_link(&mut self, neuron_src: NeuronId, neuron_dst: NeuronId) -> Option<&NewLink> {
        let innovation_idx = self.next_innovation_idx;
        self.next_innovation_idx += 1;
        let innovation = NewLink {
            innovation_idx,
            neuron_src,
            neuron_dst,
        };
        self.links.push(innovation);
        self.links.last()
    }

    pub fn find_or_add_link(
        &mut self,
        neuron_src: NeuronId,
        neuron_dst: NeuronId,
    ) -> Option<NewLink> {
        self.find_link(neuron_src, neuron_dst)
            .map(|v| v.clone())
            .or_else(|| self.add_link(neuron_src, neuron_dst).map(|v| v.clone()))
    }

    pub fn find_neuron(&self, neuron_src: NeuronId, neuron_dst: NeuronId) -> Option<&NewNeuron> {
        self.neurons
            .iter()
            .find(|i| i.neuron_src == neuron_src && i.neuron_dst == neuron_dst)
    }

    pub fn add_neuron(&mut self, neuron_src: NeuronId, neuron_dst: NeuronId) -> Option<&NewNeuron> {
        let neuron_id = self.next_neuron_id;
        self.next_neuron_id += 1;
        let neuron = NewNeuron {
            neuron_id,
            neuron_src,
            neuron_dst,
        };
        self.neurons.push(neuron);
        self.neurons.last()
    }

    pub fn find_or_add_neuron_between(
        &mut self,
        neuron_src: NeuronId,
        neuron_dst: NeuronId,
    ) -> Option<(NewLink, NewNeuron, NewLink)> {
        if neuron_src == neuron_dst {
            // unsupported
            None
        } else {
            self.find_neuron(neuron_src, neuron_dst)
                .map(|v| {
                    let link_src = self.find_link(neuron_src, v.neuron_id)
                        .expect(&format!("link_src {} -> {}", neuron_src, v.neuron_id));
                    let link_dst = self.find_link(v.neuron_id, neuron_dst)
                        .expect(&format!("link_dst {} -> {}", v.neuron_id, neuron_dst));
                    (link_src.clone(), v.clone(), link_dst.clone())
                })
                .or_else(|| {
                    self.add_neuron(neuron_src, neuron_dst)
                        .map(|v| v.clone())
                        .map(|v| {
                            let link_src = self.add_link(neuron_src, v.neuron_id)
                                .expect(&format!("link_src {} -> {}", neuron_src, v.neuron_id))
                                .clone();
                            let link_dst = self.add_link(v.neuron_id, neuron_dst)
                                .expect(&format!("link_dst {} -> {}", v.neuron_id, neuron_dst))
                                .clone();
                            (link_src, v, link_dst)
                        })
                })
        }
    }
}
