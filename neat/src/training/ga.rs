use super::crossover::{crossover, sort_best_worst};
use super::genome::Genome;
use super::innovation::InnovationRepo;
use super::mutations::{mutate, mutate_by_add_link, MutationCfg};
use super::specie::{Modifier, Specie};
use rand;
use rand::distributions::{Distribution, Sample, Uniform, Weighted, WeightedChoice};
use runtime::NeuralNetSilhouette;

#[derive(Debug, Clone)]
pub struct GeneticCfg {
    pub population_size: usize,
    pub silhouette: NeuralNetSilhouette,
    pub mutation_cfg: MutationCfg,
    pub initial_interconnections_rate: f64,
    pub bonus_young: Modifier,
    pub malus_old: Modifier,
    pub coeff_disjoint: f64,
    pub coeff_excess: f64,
    pub coeff_matched: f64,
    pub dist_threshold: f64,
}

impl Default for GeneticCfg {
    fn default() -> Self {
        GeneticCfg {
            population_size: 150,
            silhouette: NeuralNetSilhouette {
                inputs_count: 0,
                outputs_count: 0,
            },
            initial_interconnections_rate: 0.5f64,
            mutation_cfg: MutationCfg::default(),
            bonus_young: Modifier {
                threshold: 10f64,
                value: 1.3f64,
            },
            malus_old: Modifier {
                threshold: 50f64,
                value: 0.7f64,
            },
            coeff_disjoint: 1f64,
            coeff_excess: 1f64,
            coeff_matched: 0.4f64,  // to increase with population size
            dist_threshold: 3.0f64, // to increase with population size
        }
    }
}
#[derive(Debug, Clone)]
pub struct EvolutionRun {
    pub cfg: GeneticCfg,
    pub innovation_repo: InnovationRepo,
    pub epoch_count: u32,
    last_species: Vec<Specie>,
}

impl EvolutionRun {
    pub fn new(cfg: GeneticCfg) -> Self {
        let innovation_repo =
            InnovationRepo::new(cfg.silhouette.inputs_count, cfg.silhouette.outputs_count);
        EvolutionRun {
            cfg,
            innovation_repo,
            epoch_count: 0,
            last_species: vec![],
        }
    }

    pub fn epoch(
        &mut self,
        parent: Vec<Weighted<Genome>>,
        rng: &mut impl rand::Rng,
    ) -> Vec<Genome> {
        self.epoch_count += 1;
        if parent.is_empty() {
            self.random_population(rng)
        } else {
            self.next_population(parent, rng)
        }
    }

    pub fn new_fully_linked_inout(&mut self, rng: &mut impl rand::Rng) -> Genome {
        let silhouette = self.cfg.silhouette;
        let mut genome: Genome = Genome::new(silhouette.clone());
        for dst in silhouette.outputs_idx_min()..=silhouette.outputs_idx_max() {
            //link bias -> output
            if let Some(l) = self.innovation_repo.find_or_add_link(0, dst) {
                genome.add_gene_if_not_exists(
                    &l,
                    self.cfg.mutation_cfg.weights_range.sample(rng),
                    true,
                );
            }
            // link input -> output
            for src in silhouette.inputs_idx_min()..=silhouette.inputs_idx_max() {
                if let Some(l) = self.innovation_repo.find_or_add_link(src, dst) {
                    genome.add_gene_if_not_exists(
                        &l,
                        self.cfg.mutation_cfg.weights_range.sample(rng),
                        true,
                    );
                }
            }
        }
        genome
    }

    fn random_population(&mut self, rng: &mut impl rand::Rng) -> Vec<Genome> {
        let nb_link = ((self.cfg.initial_interconnections_rate
            * self.cfg.silhouette.inputs_count as f64
            * self.cfg.silhouette.outputs_count as f64) as usize)
            .max(1);
        let mut pop = (0..self.cfg.population_size - 1)
            .map(|_| {
                let mut genome: Genome = Genome::new(self.cfg.silhouette.clone());
                (0..nb_link).for_each(|_| {
                    mutate_by_add_link(
                        &mut genome,
                        &mut self.innovation_repo,
                        rng,
                        0f64,
                        self.cfg.mutation_cfg.find_loop_max_try,
                        self.cfg.mutation_cfg.add_link_max_try,
                        &self.cfg.mutation_cfg.weights_range,
                    );
                });
                genome
            })
            .collect::<Vec<_>>();
        pop.push(self.new_fully_linked_inout(rng));
        pop
    }
    //TODO If the maximum fitness of a species did not improve in 15 generations,
    // the networks in the stagnant species were not allowed to reproduce.
    fn next_population(
        &mut self,
        parents: Vec<Weighted<Genome>>,
        rng: &mut impl rand::Rng,
    ) -> Vec<Genome> {
        // TODO avoid clone;
        let mut parents2 = parents.clone();
        let mut species_pop = Specie::speciation(
            parents,
            self.last_species.clone(),
            &self.cfg.bonus_young,
            &self.cfg.malus_old,
            self.cfg.coeff_disjoint,
            self.cfg.coeff_excess,
            self.cfg.coeff_matched,
            self.cfg.dist_threshold,
            self.epoch_count,
        );
        //TODO using avg is not a good idea if weight could be negative or zero
        // because weights are adjusted
        let avg_global: f64 = species_pop
            .iter()
            .map(|(_, pop)| pop.iter().map(|wg| wg.weight as f64).sum::<f64>())
            .sum::<f64>() / (parents2.len() as f64);
        eprintln!(
            "avg_global : {:?} / species_pop.len: {:?}",
            avg_global,
            species_pop.len()
        );
        let mut offsprings_counts = if avg_global > 0f64 {
            species_pop
                .iter()
                .map(|(_, pop)| {
                    let wg_sum = pop.iter().map(|wg| wg.weight as f64).sum::<f64>();
                    let c = (wg_sum / avg_global).round() as usize;
                    //eprintln!("{:?} / {:?} -> {:?}", wg_sum, avg_global, c);
                    c
                })
                .collect::<Vec<usize>>()
        } else {
            species_pop
                .iter()
                .map(|(_, pop)| pop.len())
                .collect::<Vec<_>>()
        };
        // eprintln!(
        //     "offsprings_counts : {:?}, {:?}",
        //     offsprings_counts.len(),
        //     offsprings_counts.iter().sum::<usize>()
        // );
        while offsprings_counts.iter().sum::<usize>() > self.cfg.population_size {
            let m = offsprings_counts.iter().cloned().max();
            if let Some(max) = m {
                // println!("overpopulation : {:?}", max);
                offsprings_counts.iter_mut().for_each(|v| {
                    if *v == max {
                        *v -= 1
                    }
                });
            } else {
                break;
            }
        }
        let mut next_population = species_pop
            .iter_mut()
            .zip(offsprings_counts)
            .flat_map(|((_, old_pop), offsprings_count)| {
                if offsprings_count > 0 {
                    self.next_population_of_specie(old_pop, offsprings_count, rng)
                } else {
                    vec![]
                }
            })
            .collect::<Vec<_>>();
        //TODO find a into_iter().unzip()
        let (last_species, _): (Vec<_>, Vec<_>) = species_pop.iter().cloned().unzip();
        let missing_count = (self.cfg.population_size - next_population.len()).max(0);
        if missing_count > 0 {
            let mut missings = self.next_population_of_specie(&mut parents2, missing_count, rng);
            next_population.append(&mut missings);
        }
        self.last_species = last_species;
        next_population
    }

    fn next_population_of_specie(
        &mut self,
        parents: &mut Vec<Weighted<Genome>>,
        offsprings_size: usize,
        rng: &mut impl rand::Rng,
    ) -> Vec<Genome> {
        //only keep 1 champion per specie
        //parents.sort_by(|a, b| b.weight.cmp(&a.weight));
        //TODO MAGIC NUMBER
        let mut champions = if parents.len() < 5 {
            vec![]
        } else {
            parents
                .iter()
                .max_by_key(|e| e.weight)
                .map(|p| p.item.clone())
                .into_iter()
                .collect::<Vec<_>>()
        };
        //eprintln!("champions: {:?}", champions);
        // TODO keep best per species
        let mut offsprings = self.generate_parents(parents, offsprings_size - champions.len(), rng)
            .iter()
            .map(|p| {
                let mut genome = if p.0.item == p.1.item {
                    p.0.item.clone()
                } else {
                    let (best, worst) = sort_best_worst(&p.0, &p.1, rng);
                    crossover(&best.item, &worst.item, rng)
                };
                mutate(
                    &self.cfg.mutation_cfg,
                    &mut genome,
                    &mut self.innovation_repo,
                    rng,
                );
                genome
            })
            .collect::<Vec<_>>();
        offsprings.append(&mut champions);
        offsprings
    }

    // there are several implementation of roulette/roulette-wheel in the rust
    // (could need some upgrade)
    // But I prefere to use the WeightedChoice from rand (maybe I could contribute
    // an impl from roulette based on https://github.com/nstoddard/roulette/blob/master/src/lib.rs and http://www.keithschwarz.com/darts-dice-coins/
    fn generate_parents<'a>(
        &self,
        parents: &Vec<Weighted<Genome>>,
        nb_couple: usize,
        rng: &mut impl rand::Rng,
    ) -> Vec<(Weighted<Genome>, Weighted<Genome>)> {
        // use idx, because WeightedChoice.sample doesn't return the weight of the
        // selected
        let total_weight = parents.iter().map(|wg| wg.weight).sum::<u32>();
        let mut weighted_idx = if total_weight == 0 {
            parents
                .iter()
                .enumerate()
                .map(|(i, v)| Weighted { weight: 1, item: i })
                .collect::<Vec<_>>()
        } else {
            parents
                .iter()
                .enumerate()
                .map(|(i, v)| Weighted {
                    weight: v.weight,
                    item: i,
                })
                .collect::<Vec<_>>()
        };
        //TODO find how to use Uniform or Sample<usize> instead of WeightedChoice when
        // total_weight is 0 like Uniform::new_inclusive(0, parents.len() - 1)
        let roulette = WeightedChoice::new(weighted_idx.as_mut_slice());
        (0..nb_couple)
            .map(|_| {
                let p0 = roulette.sample(rng);
                let p1 = roulette.sample(rng);
                (parents[p0].clone(), parents[p1].clone())
            })
            .collect()
    }
}
