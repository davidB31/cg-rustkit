use super::genome::Genome;
use rand;
use rand::distributions::Weighted;
//TODO There was a 75% chance that an inherited gene was disabled if it was
// disabled in either parent.
/// let (best, worst) = sort_best_worst(dad, mum, rng).map(_.item);
pub fn crossover(best: &Genome, worst: &Genome, rng: &mut impl rand::Rng) -> Genome {
    let mut best_iter_cur = 0;
    let best_iter_max = best.count_genes();
    let mut worst_iter_cur = 0;
    let worst_iter_max = worst.count_genes();
    let mut baby = Genome::new(best.silhouette().clone());
    while best_iter_cur < best_iter_max || worst_iter_cur < worst_iter_max {
        let selected = if worst_iter_cur == worst_iter_max {
            let cur = best_iter_cur;
            best_iter_cur += 1;
            Some((cur, best))
        } else if best_iter_cur == best_iter_max {
            //TODO optimize this case (break ?)
            worst_iter_cur += 1;
            // selected is None or the last selected ?
            None
        } else {
            let best_link = best.find_gene_by_idx(best_iter_cur)
                .expect("best should be defined");
            let worst_link = worst
                .find_gene_by_idx(worst_iter_cur)
                .expect("worst should be defined");
            if best_link.innovation_idx() < worst_link.innovation_idx() {
                let cur = best_iter_cur;
                best_iter_cur += 1;
                Some((cur, best))
            } else if best_link.innovation_idx() > worst_link.innovation_idx() {
                //TODO optimize this case (continue ?)
                worst_iter_cur += 1;
                None
            } else {
                let cur = if rng.gen_range(0f64, 1f64) < 0.5 {
                    Some((best_iter_cur, best))
                } else {
                    Some((worst_iter_cur, worst))
                };
                best_iter_cur += 1;
                worst_iter_cur += 1;
                cur
            }
        };
        //Check if we already have the neurons referred to in SelectedGene.
        //If not, they need to be added.
        if let Some((gene_idx, src)) = selected {
            //eprintln!("gene {} {}", gene_idx, src.count_genes());
            baby.add_gene(
                src.find_gene_by_idx(gene_idx)
                    .expect("gene should exist")
                    .clone(),
            );
        }
    }
    baby
}

pub fn sort_best_worst<'a>(
    dad: &'a Weighted<Genome>,
    mum: &'a Weighted<Genome>,
    rng: &mut impl rand::Rng,
) -> (&'a Weighted<Genome>, &'a Weighted<Genome>) {
    if mum.weight > dad.weight {
        (&mum, &dad)
    } else if mum.weight < dad.weight {
        (&dad, &mum)
    } else if mum.item.count_genes() > dad.item.count_genes() {
        (&mum, &dad)
    } else if mum.item.count_genes() < dad.item.count_genes() {
        (&dad, &mum)
    } else if rng.gen_range(0f64, 1f64) < 0.5 {
        (&mum, &dad)
    } else {
        (&dad, &mum)
    }
}

#[cfg(test)]
mod tests {
    use super::super::innovation::InnovationRepo;
    use super::*;
    use spectral::prelude::*;

    fn make_data() -> (InnovationRepo, Genome, Genome) {
        let mut innovation_repo = InnovationRepo::new(3, 1);
        let l14 = innovation_repo.add_link(1, 4).expect("created").clone();
        let l24 = innovation_repo.add_link(2, 4).expect("created").clone();
        let l34 = innovation_repo.add_link(2, 4).expect("created").clone();
        let l25 = innovation_repo.add_link(2, 4).expect("created").clone();
        let l54 = innovation_repo.add_link(2, 4).expect("created").clone();
        let l37 = innovation_repo.add_link(2, 4).expect("created").clone();
        let l74 = innovation_repo.add_link(2, 4).expect("created").clone();
        let l59 = innovation_repo.add_link(2, 4).expect("created").clone();
        let l94 = innovation_repo.add_link(2, 4).expect("created").clone();
        //let l66 = innovation_repo.add_link(2, 4).expect("created").clone();
        //let l77 = innovation_repo.add_link(2, 4).expect("created").clone();
        let l17 = innovation_repo.add_link(2, 4).expect("created").clone();
        let l39 = innovation_repo.add_link(2, 4).expect("created").clone();

        let mut genome_a: Genome = Default::default();
        genome_a.add_gene_if_not_exists(&l14, 1.0f64, false);
        genome_a.add_gene_if_not_exists(&l24, 1.0f64, true);
        genome_a.add_gene_if_not_exists(&l34, 1.0f64, false);
        genome_a.add_gene_if_not_exists(&l37, 1.0f64, true);
        genome_a.add_gene_if_not_exists(&l74, 1.0f64, true);
        genome_a.add_gene_if_not_exists(&l17, 1.0f64, true);

        let mut genome_b: Genome = Default::default();
        genome_b.add_gene_if_not_exists(&l14, 2.0f64, true);
        genome_b.add_gene_if_not_exists(&l24, 2.0f64, false);
        genome_b.add_gene_if_not_exists(&l34, 2.0f64, false);
        genome_b.add_gene_if_not_exists(&l25, 2.0f64, true);
        genome_b.add_gene_if_not_exists(&l54, 2.0f64, false);
        genome_b.add_gene_if_not_exists(&l59, 2.0f64, true);
        genome_b.add_gene_if_not_exists(&l94, 2.0f64, true);
        genome_b.add_gene_if_not_exists(&l39, 2.0f64, true);

        (innovation_repo, genome_a, genome_b)
    }

    #[test]
    fn test_crossover_longer_best() {
        let mut rng = rand::thread_rng();
        let (_innovation_repo, genome_worst, genome_best) = make_data();
        let actual = crossover(&genome_best, &genome_worst, &mut rng);
        assert_that!(&actual.count_genes()).is_equal_to(&genome_best.count_genes());
        assert_that!(&actual.count_genes()).is_greater_than(&genome_worst.count_genes());
        for i in 0..actual.count_genes() {
            let og_actual = actual.find_gene_by_idx(i);
            let og_best = genome_best.find_gene_by_idx(i);
            assert_that!(&og_actual.map(|v| v.innovation_idx()))
                .is_equal_to(&og_best.map(|v| v.innovation_idx()));
        }
    }

    #[test]
    fn test_crossover_shorter_best() {
        let mut rng = rand::thread_rng();
        let (_innovation_repo, genome_best, genome_worst) = make_data();
        let actual = crossover(&genome_best, &genome_worst, &mut rng);
        assert_that!(&actual.count_genes()).is_equal_to(&genome_best.count_genes());
        assert_that!(&actual.count_genes()).is_less_than(&genome_worst.count_genes());
        for i in 0..actual.count_genes() {
            let og_actual = actual.find_gene_by_idx(i);
            let og_best = genome_best.find_gene_by_idx(i);
            assert_that!(&og_actual.map(|v| v.innovation_idx()))
                .is_equal_to(&og_best.map(|v| v.innovation_idx()));
        }
    }
}
