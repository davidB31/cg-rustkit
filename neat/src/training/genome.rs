use super::innovation::NewLink;
use super::InnovationIdx;
use super::NeuronId;
use runtime::Link;
use runtime::NeuralNetGraphDef;
use runtime::NeuralNetSilhouette;

#[derive(Debug, Clone, PartialEq)]
pub struct GeneLink {
    pub weight: f64,
    pub is_enabled: bool,
    //readonly
    innovation_idx: InnovationIdx,
    neuron_src: NeuronId,
    neuron_dst: NeuronId,
}

impl GeneLink {
    pub fn innovation_idx(&self) -> InnovationIdx {
        self.innovation_idx
    }
}

// several Genome are based on the same InnovationRepo
#[derive(Debug, Clone, PartialEq, Default)]
pub struct Genome {
    genes: Vec<GeneLink>,
    silhouette: NeuralNetSilhouette,
    /* pub innovation: Rc<InnovationRepo> */
    /* cache list of neuron_id */
}

// impl Default for Genome {
//     fn default() -> Self {
//         Genome {
//             fitness: 0f64,
//             genes: vec![],
//         }
//     }
// }

impl Genome {
    pub fn new(silhouette: NeuralNetSilhouette) -> Self {
        Genome {
            genes: vec![],
            silhouette,
        }
    }

    pub fn silhouette(&self) -> &NeuralNetSilhouette {
        &self.silhouette
    }

    pub fn add_gene(&mut self, gene: GeneLink) {
        self.genes.push(gene);
        self.genes.sort_by_key(|g| g.innovation_idx)
    }

    pub fn add_gene_if_not_exists(&mut self, innovation: &NewLink, weight: f64, is_enabled: bool) {
        if !self.genes
            .iter()
            .find(|g| g.innovation_idx == innovation.innovation_idx)
            .is_some()
        {
            self.add_gene(GeneLink {
                weight: weight.into(),
                is_enabled,
                innovation_idx: innovation.innovation_idx,
                neuron_src: innovation.neuron_src,
                neuron_dst: innovation.neuron_dst,
            });
        }
    }

    pub fn count_genes(&self) -> usize {
        self.genes.len()
    }

    pub fn find_gene_by_idx(&self, idx: usize) -> Option<&GeneLink> {
        self.genes.get(idx)
    }

    pub fn find_gene_by_idx_mut(&mut self, idx: usize) -> Option<&mut GeneLink> {
        self.genes.get_mut(idx)
    }

    pub fn list_neuron_ids(&self) -> Vec<NeuronId> {
        let mut res = self.genes
            .iter()
            .filter(|v| v.is_enabled)
            .flat_map(|v| vec![v.neuron_src, v.neuron_dst])
            .map(|v| v.clone())
            .chain(self.silhouette.bias_idx()..=self.silhouette.inputs_idx_max())
            .chain(self.silhouette.inputs_idx_min()..=self.silhouette.inputs_idx_max())
            .chain(self.silhouette.outputs_idx_min()..=self.silhouette.outputs_idx_max())
            .collect::<Vec<_>>();
        res.sort();
        res.dedup();
        res
    }

    pub fn has_link_between(&self, neuron_src: NeuronId, neuron_dst: NeuronId) -> bool {
        self.genes
            .iter()
            .find(|l| l.neuron_src == neuron_src && l.neuron_dst == neuron_dst)
            .is_some()
    }
}

impl<'a> From<&'a Genome> for NeuralNetGraphDef {
    fn from(v: &Genome) -> Self {
        let links = v.genes
            .iter()
            .flat_map(|g| {
                if !g.is_enabled {
                    None
                } else {
                    Some(Link {
                        src: g.neuron_src,
                        dst: g.neuron_dst,
                        weight: g.weight.into(),
                    })
                }
            })
            .collect();
        NeuralNetGraphDef {
            silhouette: v.silhouette.clone(),
            links,
            //FIXME compute number of activations
            activations_responses: vec![1f64 / 4.924273f64; 100],
            ..Default::default()
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use spectral::prelude::*;

    #[test]
    fn test_list_neuron_ids() {
        let v = Genome::new(NeuralNetSilhouette {
            inputs_count: 2,
            outputs_count: 1,
        });
        assert_that!(&v.list_neuron_ids()).is_equal_to(vec![0, 1, 2, 3]);
    }
}
