use super::genome::Genome;
use super::innovation::InnovationRepo;
use super::NeuronId;
use rand;
use rand::distributions::Distribution;
use rand::distributions::Uniform;

#[derive(Debug, Clone)]
pub struct MutationCfg {
    pub add_link_rate: f64,
    pub recurrent_rate: f64,
    pub find_loop_max_try: u8,
    pub add_link_max_try: u8,
    pub num_permitted_neurons_max: u8,
    pub add_neuron_rate: f64,
    pub find_old_link_max_try: u8,
    pub mutate_weights_rate: f64,
    pub weights_range: Uniform<f64>,
    pub weights_new_rate: f64,
    pub weights_perturbation_max: f64,
}

//TODO In smaller populations, the probability of adding a new node was 0.03
// and the probability of a new link mutation was 0.05. In the larger
// population, the probability of adding a new link was 0.3, because a larger
// population can tolerate a larger number of prospective species and greater
// topological diversity.
// Links need to be added significantly more often than nodes, and an average
// weight difference of 3.0 is about as significant as one disjoint or excess
// gene.
impl Default for MutationCfg {
    fn default() -> Self {
        MutationCfg {
            add_link_rate: 0.05f64,
            recurrent_rate: 0f64,
            find_loop_max_try: 2u8,
            add_link_max_try: 2u8,
            num_permitted_neurons_max: 6u8,
            add_neuron_rate: 0.03f64,
            find_old_link_max_try: 3u8,
            mutate_weights_rate: 0.8f64,
            weights_range: Uniform::new_inclusive(0f64, 1f64),
            weights_new_rate: 0.1f64,
            weights_perturbation_max: 0.5f64,
        }
    }
}

// return false if no mutation applied
pub fn mutate(
    cfg: &MutationCfg,
    genome: &mut Genome,
    innovation_repo: &mut InnovationRepo,
    rng: &mut impl rand::Rng,
) -> bool {
    let mut mutated = false;
    if genome.list_neuron_ids().len() < cfg.num_permitted_neurons_max as usize
        && rng.gen_range(0f64, 1f64) <= cfg.add_neuron_rate
    {
        mutated = mutate_by_add_neuron(genome, innovation_repo, rng, cfg.find_old_link_max_try)
            || mutated;
    }
    if rng.gen_range(0f64, 1f64) <= cfg.add_link_rate {
        mutated = mutate_by_add_link(
            genome,
            innovation_repo,
            rng,
            cfg.recurrent_rate,
            cfg.find_loop_max_try,
            cfg.add_link_max_try,
            &cfg.weights_range,
        ) || mutated;
    }
    if rng.gen_range(0f64, 1f64) <= cfg.mutate_weights_rate {
        mutated = mutate_weights(
            genome,
            rng,
            cfg.mutate_weights_rate,
            &cfg.weights_range,
            cfg.weights_new_rate,
            cfg.weights_perturbation_max,
        ) || mutated;
    }
    mutated
}

/// return false if no link was added
pub fn mutate_by_add_link(
    genome: &mut Genome,
    innovation_repo: &mut InnovationRepo,
    rng: &mut impl rand::Rng,
    recurrent_rate: f64,
    find_loop_max_try: u8,
    add_link_max_try: u8,
    weights_range: &impl Distribution<f64>,
) -> bool {
    // if rng.gen_range(0f64, 1f64) > mut_rate {
    //     return false;
    // }
    let neuron_ids = if rng.gen_range(0f64, 1f64) < recurrent_rate {
        find_available_recurrent_link(&genome, rng, find_loop_max_try)
    } else {
        find_available_non_recurrent_link(&genome, rng, add_link_max_try)
    };
    neuron_ids.map_or(false, |(src, dst)| {
        add_link(genome, innovation_repo, src, dst, weights_range.sample(rng))
    })
}

fn add_link(
    genome: &mut Genome,
    innovation_repo: &mut InnovationRepo,
    src: NeuronId,
    dst: NeuronId,
    weight: f64,
) -> bool {
    innovation_repo
        .find_or_add_link(src, dst)
        .map(|link| {
            genome.add_gene_if_not_exists(&link, weight, true);
            true
        })
        .unwrap_or(false)
}

fn find_available_recurrent_link(
    genome: &Genome,
    rng: &mut impl rand::Rng,
    find_loop_max_try: u8,
) -> Option<(NeuronId, NeuronId)> {
    let available_neurons_id = genome.list_neuron_ids();

    //? not a clean usage of "find" to do side-effect
    (0..find_loop_max_try)
        .flat_map(|_| {
            let neuron_pos = rng.gen_range(
                genome.silhouette().hidden_idx_min(),
                available_neurons_id.len() - 1,
            );
            let neuron_id = available_neurons_id[neuron_pos];
            if !genome.has_link_between(neuron_id, neuron_id)
                && !genome.silhouette().is_input(neuron_id)
                && !genome.silhouette().is_bias(neuron_id)
            {
                Some((neuron_id, neuron_id))
            } else {
                None
            }
        })
        .nth(0)
}

fn find_available_non_recurrent_link(
    genome: &Genome,
    rng: &mut impl rand::Rng,
    add_link_max_try: u8,
) -> Option<(NeuronId, NeuronId)> {
    let available_neurons_id = genome.list_neuron_ids();
    // try to find two unlinked neurons. Make add_link_max_try attempts
    //TODO should we allow link from output nodes ?
    (0..add_link_max_try)
        .flat_map(|_| {
            let pos1 = rng.gen_range(0, available_neurons_id.len() - 1);
            let neuron1 = available_neurons_id[pos1];
            let pos2 = if (available_neurons_id.len() - 1) == genome.silhouette().outputs_idx_min()
            {
                genome.silhouette().outputs_idx_min()
            } else {
                rng.gen_range(
                    genome.silhouette().outputs_idx_min(),
                    available_neurons_id.len() - 1,
                )
            };
            let neuron2 = available_neurons_id[pos2];
            if neuron1 != neuron2 && !genome.has_link_between(neuron1, neuron2) {
                Some((neuron1, neuron2))
            } else {
                None
            }
        })
        .nth(0)
}

fn gene_is_selectable(
    genome: &Genome,
    innovation_repo: &InnovationRepo,
    gene_idx: usize,
) -> Option<usize> {
    genome
        .find_gene_by_idx(gene_idx)
        .iter()
        .filter(|gene| {
            gene.is_enabled
                && !innovation_repo
                    .find_link_by_idx(gene.innovation_idx())
                    .map(|l| l.is_recurrent())
                    .unwrap_or(false)
        })
        .nth(0)
        .map(|_| gene_idx)
}

fn split_link_with_neuron(
    genome: &mut Genome,
    innovation_repo: &mut InnovationRepo,
    gene_pos: usize,
) {
    if let Some(gene) = genome.find_gene_by_idx(gene_pos).map(|v| v.clone()) {
        let is_enabled = innovation_repo
            .find_link_by_idx(gene.innovation_idx())
            .map(|v| v.clone())
            .and_then(|link| {
                innovation_repo.find_or_add_neuron_between(link.neuron_src, link.neuron_dst)
            })
            .map(|(link_before, _, link_after)| {
                genome.add_gene_if_not_exists(&link_before, gene.weight, true);
                genome.add_gene_if_not_exists(&link_after, 1.0f64, true);
                false
            })
            .unwrap_or(true);
        genome
            .find_gene_by_idx_mut(gene_pos)
            .map(|gene| gene.is_enabled = is_enabled);
    }
}

fn mutate_by_add_neuron(
    genome: &mut Genome,
    innovation_repo: &mut InnovationRepo,
    rng: &mut impl rand::Rng,
    find_old_link_max_try: u8,
) -> bool {
    // if rng.gen_range(0f64, 1f64) > mut_rate {
    //     return false;
    // }
    //first a link is chosen to split. If the genome is small the code makes
    //sure one of the older links is split to ensure a chaining effect does
    //not occur. Here, if the genome contains less than 5 hidden neurons it
    //is considered to be too small to select a link at random.
    let size_threshold = genome.silhouette().hidden_idx_min() + 5;
    let available_neurons_id = genome.list_neuron_ids();
    let old_link_first = available_neurons_id.len() < size_threshold;

    let num_genes = genome.count_genes() as usize;
    if num_genes < 2 {
        false
    } else {
        (0..num_genes * 10)
            .flat_map(|i| {
                let upper = if old_link_first && i < find_old_link_max_try as usize {
                    //choose a link with a bias towards the older links in the genome
                    (num_genes as f64 - 1f64 - (num_genes as f64).sqrt()).max(1f64) as usize
                } else {
                    num_genes - 1
                };
                let select = rng.gen_range(0, upper);
                gene_is_selectable(genome, innovation_repo, select)
            })
            .nth(0)
            .map_or(false, |link_pos| {
                split_link_with_neuron(genome, innovation_repo, link_pos);
                true
            })
    }
}

fn mutate_weights(
    genome: &mut Genome,
    rng: &mut impl rand::Rng,
    mut_rate: f64,
    weights_range: &impl Distribution<f64>,
    weights_new_rate: f64,
    perturbation_max: f64,
) -> bool {
    // use find_gene_by_idx_mut
    //genome.genes.iter_mut().fold(|acc, g|{
    let mut mutated = false;
    for i in 0..genome.count_genes() {
        if rng.gen_range(0f64, 1f64) <= mut_rate {
            let g = genome.find_gene_by_idx_mut(i).expect("gene expected");
            let weight = if rng.gen_range(0f64, 1f64) <= weights_new_rate {
                weights_range.sample(rng)
            } else {
                g.weight + rng.gen_range(-perturbation_max, perturbation_max)
            };
            g.weight = weight.into();
            mutated = mutated || true;
        }
    }
    mutated
}

//TODO mutate_activation
// fn mutate_activation_response(genome: &mut Genome, perturbation_max: f64) {
//     unimplemented!()
// }
