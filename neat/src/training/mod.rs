mod crossover;
mod ga;
mod genome;
mod innovation;
mod mutations;
mod specie;

pub use self::ga::*;
pub use self::genome::*;
pub use self::innovation::*;
pub use self::mutations::MutationCfg;

pub type InnovationIdx = usize;
pub type NeuronId = usize;

//TODO use specie
//TODO
//TODO mutation on enable/disable
//TODO remove need of innovationRepo in every place (where innovationRepo is
// not mutated, eg by copy innovation instead od just ID) TODO integrate with
// carpol (of openai gym ?) FIXME training_xor like
//TODO provide configuration preset
//TODO update README (add link, documentation,...)
//TODO review all code and compare with other impl, doc, book, papers
//TODO exporter to display network with vega (or d3js ?)
//TODO In each generation, 25% of offspring resulted from mutation without
// crossover. The interspecies mating rate was 0.001.
