#[macro_use]
#[cfg(test)]
extern crate spectral;
extern crate rand;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;

pub mod exporters;
pub mod runtime;
pub mod training;

// TODO
// - define bias as a node + link (vs attr of node) to ease crossover,...
// always node 0, no recurrent, constant output to 1.0 * replace expect(...) by
// failure * refactor InnovationRepo (find_or_create_neuronId_between(from, to))
// - InnovationId only for link
// - being able to create the phenotype for number of input, number of output,
// list of link
