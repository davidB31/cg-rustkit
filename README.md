# Resources

## Tools

* [cg-brutaltester](https://github.com/dreignier/cg-brutaltester)
* [CodinGame statistic tool](http://cgstats.magusgeek.com/)
* https://forum.codingame.com/t/cg-local/10359
* https://github.com/s-vivien/CGBenchmark
* https://github.com/lpenz/rust-sourcebundler
* [tweak parameters for CodinGame bots](https://github.com/eulerscheZahl/ParameterFiddler)
* [Generic combinations generator in Rust with iterator.](https://gist.github.com/ioncodes/3401ebc8c71bd41638e483e96d2d40f1)

## Post-mortem

* https://github.com/Agade09/Agade-Code-4-Life-Postmortem/blob/master/Agade_C4L_Postmortem.md

## TODO

* maybe extract some folder as own project using ```git subtree split -P xxx -b xxxx-only``` see [git subtree - Detach (move) subdirectory into separate Git repository - Stack Overflow](https://stackoverflow.com/questions/359424/detach-move-subdirectory-into-separate-git-repository/17864475#17864475)