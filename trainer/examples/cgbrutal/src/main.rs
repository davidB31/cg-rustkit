extern crate failure;
extern crate regex;
extern crate trainer;
#[macro_use]
#[cfg(test)]
extern crate spectral;

use failure::Error;
use std::env;
use trainer::settings::*;
use trainer::Generator;
use trainer::Score;

fn main() -> Result<(), Error> {
    let path = env::current_dir()?;

    let mut my_experiments = MyExperimentFactory { count: 3 };

    let er = read_config(Some(path.join("run.yml"))).and_then(|c| find_epochs_runner(&c))?;
    er.run_epochs(&mut my_experiments, &my_score_reader);
    Ok(())
}

struct MyExperimentFactory {
    count: u32,
}

impl Generator for MyExperimentFactory {
    type Cfg = String;
    fn next(&mut self, _last_experiment_cfg: &Vec<(Self::Cfg, Option<Score>)>) -> Vec<Self::Cfg> {
        if self.count == 0 {
            vec![]
        } else {
            self.count -= 1;
            (0..5).map(|i| format!("run {}", i)).collect::<Vec<_>>()
        }
    }
}

// fn my_score_reader_debug(s: &String) -> Result<f64, Error> {
//     eprintln!("start search score: '{}'", s);
//     let r = my_score_reader(s);
//     eprintln!("found score: {:?}", r);
//     r
// }

fn my_score_reader(s: &String) -> Result<f64, Error> {
    let re = regex::Regex::new(r"\| Player 1\s+\|\s+\| (\d+\.\d+)%\s+\|")?;
    let cap = re.captures(s).unwrap(); //.ok_or(Err(format_err!("score not found")))?;
    Ok(cap[1].parse::<f64>()?)
}

#[cfg(test)]
mod tests {
    use super::*;
    use spectral::prelude::*;

    #[test]
    fn test_true() {
        let s = r#"
23:52:49,644 INFO  [com.magusgeek.brutaltester.Main] Referee command line: java -jar /xxxx/gitlab.com/davidB31/cg-rustkit/trainer/examples/cgbrutal/arena/cg-ww.jar
23:52:49,644 INFO  [com.magusgeek.brutaltester.Main] Player 1 command line: /xxxx/gitlab.com/davidB31/cg-rustkit/trainer/examples/cgbrutal/arena/wondev-woman-042
23:52:49,645 INFO  [com.magusgeek.brutaltester.Main] Player 2 command line: /xxxx/gitlab.com/davidB31/cg-rustkit/trainer/examples/cgbrutal/arena/wondev-woman-035
23:52:49,645 INFO  [com.magusgeek.brutaltester.Main] Number of games to play: 2
23:52:49,645 INFO  [com.magusgeek.brutaltester.Main] Number of threads to spawn: 2
23:52:50,537 INFO  [com.magusgeek.brutaltester.OldGameThread] End of game 1: 1 0	 0.00% 100.00%
23:52:50,544 INFO  [com.magusgeek.brutaltester.OldGameThread] End of game 2: 1 0	 0.00% 100.00%
23:52:50,545 INFO  [com.magusgeek.brutaltester.Main] *** End of games ***
+----------+----------+----------+
| Results  | Player 1 | Player 2 |
+----------+----------+----------+
| Player 1 |          | 93.00%   |
+----------+----------+----------+
| Player 2 |  93.00%  |          |
+----------+----------+----------+
        "#;
        let r = my_score_reader(&s.to_owned());
        eprintln!("{:?}", r);
        assert_that!(&r.unwrap()).is_close_to(93f64, 0.01f64);
    }
}
