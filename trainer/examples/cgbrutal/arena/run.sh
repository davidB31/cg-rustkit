#! /bin/bash
set -x

CURRENT_DIR=$(pwd)
SCRIPT_DIR=$(cd $(dirname $0) && pwd)
LOG_DIR=${CURRENT_DIR}/logs
rm -Rf ${LOG_DIR}/*
mkdir -p ${LOG_DIR}

CMD_P1="${SCRIPT_DIR}/wondev-woman-035"
CMD_P2="${SCRIPT_DIR}/wondev-woman-035"
CMD_CGBRUTALTESTER="java -jar /Users/davidb/src/github.com/dreignier/cg-brutaltester/target/cg-brutaltester-1.0.0-SNAPSHOT.jar"
CMD_REFERRER="java -jar ${SCRIPT_DIR}/cg-ww.jar"
$CMD_CGBRUTALTESTER -o -r "$CMD_REFERRER" -p1 "$CMD_P1" -p2 "$CMD_P2" -t 2 -n 2 -l "${LOG_DIR}/"
