extern crate failure;

extern crate trainer;

use std::env;
use trainer::Generator;
use trainer::{EpochsRunner, Score};

fn main() -> std::io::Result<()> {
    let path = env::current_dir()?;

    let mut my_experiments = MyExperimentFactory { count: 3 };

    let er = EpochsRunner {
        folder: path.join("run"),
        cmd_tmpl: "echo $(pwd) && echo 0".to_owned(),
    };
    er.run_epochs(&mut my_experiments, &my_score_reader);
    Ok(())
}

struct MyExperimentFactory {
    count: u32,
}

impl Generator for MyExperimentFactory {
    type Cfg = String;
    fn next(&mut self, _last_experiment_cfg: &Vec<(Self::Cfg, Option<Score>)>) -> Vec<Self::Cfg> {
        if self.count == 0 {
            vec![]
        } else {
            self.count -= 1;
            (0..5).map(|i| format!("run {}", i)).collect::<Vec<_>>()
        }
    }
}

fn my_score_reader(_s: &String) -> Result<f64, Error> {
    Ok(0f64)
}
