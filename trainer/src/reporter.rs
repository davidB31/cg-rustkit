use super::Experiment;
use super::Score;
use chrono;
use csv;
use std::path::Path;

#[derive(Debug, Clone, Serialize)]
struct ResultRecord {
    pub epoch_index: u32,
    pub experiment_index: u32,
    pub score: Score,
    pub start_time: chrono::DateTime<chrono::Utc>,
    /// duration in millis
    pub duration: u64,
}

pub fn export_results_csv(folder: &Path, data: &Vec<Option<Experiment>>) {
    let records = data
        .iter()
        .flat_map(|re| re)
        .map(|exp| ResultRecord {
            epoch_index: exp.epoch_index,
            experiment_index: exp.index,
            score: exp.score,
            start_time: exp.start_time.into(),
            duration: chrono::Duration::from_std(exp.duration)
                .expect("duration cnv")
                .num_milliseconds() as u64,
        })
        .collect::<Vec<_>>();
    let mut wtr = csv::Writer::from_path(folder.join("results.csv")).expect("to open results.csv");
    records.iter().for_each(|record| {
        // When writing records with Serde using structs, the header row is written
        // automatically.
        wtr.serialize(record).expect("to write record");
    });
    wtr.flush().expect("to flush results");
}
