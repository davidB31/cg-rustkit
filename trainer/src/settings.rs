use super::EpochsRunner;
use config::Config;
use config::Environment;
use config::File;
use failure::Error;
use std::path::PathBuf;

pub fn read_config(filename: Option<PathBuf>) -> Result<Config, Error> {
    let mut s = Config::default();
    s.merge(File::with_name("default").required(false))?;
    if let Some(f) = filename {
        s.merge(File::from(f).required(false))?;
    }
    // Add in settings from the environment (with a prefix of APP)
    // Eg.. `APP_DEBUG=1 ./target/app` would set the `debug` key
    s.merge(Environment::with_prefix("APP"))?;
    Ok(s)
}

/// cfg.get::<EpochsRunner>("epochs_runner")
pub fn find_epochs_runner(cfg: &Config) -> Result<EpochsRunner, Error> {
    let s = cfg.get::<EpochsRunner>("epochs_runner")?;
    Ok(s)
}
