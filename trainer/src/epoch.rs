use failure::Error;
use std::fmt::Display;
use std::fs;
use std::fs::File;
use std::io::prelude::*;
use std::path::PathBuf;
use std::time::{Duration, SystemTime};

pub type Score = f64;
pub type ScoreRead = Fn(&String) -> Result<Score, Error>;

//TODO store score_reader: ScoreRead, as part of EpochsRunner ?
#[derive(Debug, Deserialize)]
pub struct EpochsRunner {
    pub folder: PathBuf,
    pub cmd_tmpl: String,
}

#[derive(Debug, Clone)]
struct Epoch {
    index: u32,
    folder: PathBuf,
    cmd: String,
}

#[derive(Debug, Clone)]
pub struct Experiment {
    pub start_time: SystemTime,
    pub duration: Duration,
    pub epoch_index: u32,
    pub index: u32,
    pub folder: PathBuf,
    pub score: Score,
}

pub trait Generator {
    type Cfg: Clone + Display;

    fn next(&mut self, last_experiment_cfg: &Vec<(Self::Cfg, Option<Score>)>) -> Vec<Self::Cfg>;
}

impl EpochsRunner {
    pub fn run_epochs(
        &self,
        generator: &mut impl Generator,
        score_reader: &ScoreRead,
    ) -> Vec<Option<Experiment>> {
        let mut run_results = vec![];
        let mut epoch_index = 0;
        let mut last_experiment_cfg = vec![];
        loop {
            let cfgs = generator.next(&last_experiment_cfg);
            if cfgs.is_empty() {
                break;
            }
            let epoch_folder = self.folder.join(format!("epoch_{:04}", epoch_index));
            fs::create_dir_all(&epoch_folder).expect("create folder for epoch");
            let epoch = Epoch {
                index: epoch_index,
                folder: epoch_folder,
                cmd: self.cmd_tmpl.clone(),
            };
            epoch.setup();
            let results = epoch
                .run(&cfgs, score_reader)
                .into_iter()
                .map(|r| r.ok())
                .collect::<Vec<_>>();
            super::reporter::export_results_csv(&epoch.folder, &results);
            last_experiment_cfg = cfgs
                .iter()
                .zip(&results)
                .map(|(c, r)| (c.clone(), r.clone().map(|v| v.score)))
                .collect::<Vec<_>>();
            results.into_iter().for_each(|r| run_results.push(r));
            epoch_index += 1;
        }
        super::reporter::export_results_csv(&self.folder, &run_results);
        run_results
    }
}

impl Epoch {
    fn setup(&self) {}

    fn run<Cfg>(
        &self,
        experiment_cfgs: &Vec<Cfg>,
        score_reader: &ScoreRead,
    ) -> Vec<Result<Experiment, Error>>
    where
        Cfg: Display,
    {
        experiment_cfgs
            .iter()
            .enumerate()
            .map(|(i, cfg)| self.run_experiment(i as u32, cfg, score_reader))
            .collect()
    }

    fn run_experiment<Cfg>(
        &self,
        index: u32,
        cfg: &Cfg,
        score_reader: &ScoreRead,
    ) -> Result<Experiment, Error>
    where
        Cfg: Display,
    {
        use subprocess::Exec;
        use subprocess::Redirection;

        let folder: PathBuf = self.folder.join(format!("experiment_{:04}", index));
        fs::create_dir_all(&folder)?;
        {
            let mut buffer = File::create(folder.join("cfg.txt"))?;
            write!(buffer, "{}", cfg)?;
            buffer.flush()?;
        }

        let start_time = SystemTime::now();
        {
            let stdout_file = File::create(folder.join("stdout.txt"))?;
            let stderr_file = File::create(folder.join("stderr.txt"))?;
            Exec::shell(&self.cmd)
                .cwd(&folder)
                .stdout(Redirection::File(stdout_file))
                .stderr(Redirection::File(stderr_file))
                .join()?;
            //.capture()?.stdout_str()
        }
        let duration = SystemTime::now().duration_since(start_time)?;
        let score = {
            // TODO only read the last 1MB (file.seek)
            // see https://github.com/amcoder/tail-rust/blob/master/src/main.rs
            // let bytes_to_read = min(bytes_left, BUFFER_SIZE as u64) as uint;
            // Read the next block of the file
            // try!(file.seek(-((bytes_to_read + bytes_read) as i64), SeekCur));
            let mut stdout_file_r = File::open(folder.join("stdout.txt"))?;
            let mut output = String::new();
            stdout_file_r.read_to_string(&mut output)?;
            score_reader(&output)?
        };
        Ok(Experiment {
            start_time,
            duration,
            epoch_index: self.index,
            index,
            folder: folder,
            score,
        })
    }
}
