# Trainer

A trainer to run several experimentations.

## Goals

### short

- tuning (hyper)parameters of bot for CodinGame. (each experimentations could be a launch of cg-brutaltester)
- run evolution (epoch) for AI (I'm learning)

### long

- using to tune OpenGym
- intergration with ML tooling

## Usage

### Configuration

### Result

A directory layout as
```
run_<ts_start>/
  run.cfg # the configuration used for this run
  results.csv #the aggregated **/results.tsv
  epoch_<index>/
    results.csv
    experiment_<index>/
      cfg.txt
      output.txt
      ...
```

- indexes are formatted with 4 digit into directory name

#### results.csv

A comma separated values, with column name on first row. The columns are:

- epoch_index: positive integer (u32)
- experiment_index: positive interger (u32)
_ score: f64
- start time: string formated ISO 8601
- duration: (u64) in millis

Tips: use [xsv - Cargo: packages for Rust](https://crates.io/crates/xsv) to browse, to transform csv file

## TODO

- [X] generation a directory layout
- [X] store result in csv
- [X] generate configuration of every experiment
- [X] run the process into experiment directory
- [ ] read configuration from file (no cli)
- [ ] use cg-brutaltester for experiment
- [ ] run experiment concurrently
- [ ] integration with my neat lib to manage evolution
  - [ ] save + load genome
  - [ ] load new genome and play with it
  - [ ] if no genome then generate a random population
  - [ ] if no more genome to evaluate then start a new generation : generate a new population
- [ ] documentation
- [ ] having tool, doc to plot (experiment/population, score, epoch/generation)
- [ ] having a way to replay, to view a experiment (visual check)
- [ ] integration with ML tooling (notebooks, tensorboard, ...)
