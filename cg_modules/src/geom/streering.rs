use v2::*;

fn next_mvt(pos_current: &V2, pos_target: &V2, speed_current: &V2, max_speed: f64) -> V2 {
    let desired_mvt = (pos_target - pos_current).to_length(max_speed);
    let mvt = desired_mvt - speed_current;
    mvt
}
