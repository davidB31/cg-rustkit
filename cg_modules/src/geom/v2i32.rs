//#![allow(dead_code)]
//#![allow(unused_variables)]
//#![allow(unused_mut)]
//#![allow(unused_imports)]

use std;
use std::convert::From;
use std::hash::{Hash, Hasher};
use std::ops::Add;
use std::ops::AddAssign;
use std::ops::Sub;
use std::ops::SubAssign;

fn main() {
    println!("{:?}", V2 { x: 1i32, y: 3i32 } + V2 { x: 2i32, y: 4i32 });
}

#[derive(Clone, Copy, Debug, PartialEq, Hash, Eq)]
pub struct V2 {
    pub x: i32,
    pub y: i32,
}

// impl Hash for V2 {
//     fn hash<H: Hasher>(&self, state: &mut H) {
//         ((self.x * 100i32) as i64).hash(state);
//         ((self.y * 100i32) as i64).hash(state);
//     }
// }
// impl Eq for V2 {}

//manathan
impl V2 {
    pub fn new0() -> Self {
        V2::new(0i32, 0i32)
    }

    pub fn new(x: i32, y: i32) -> Self {
        V2 { x: x, y: y }
    }

    pub fn dist(&self, p: &V2) -> i32 {
        (self.x - p.x).abs() + (self.y - p.y).abs()
    }
    pub fn dist_8x(&self, p: &V2) -> i32 {
        (self.x - p.x).abs().max((self.y - p.y).abs())
    }

    pub fn length(&self) -> i32 {
        self.x.abs() + self.y.abs()
    }

    pub fn minus(&mut self, p: &V2) -> &mut Self {
        self.x -= p.x;
        self.y -= p.y;
        self
    }

    pub fn plus(&mut self, p: &V2) -> &mut Self {
        self.x += p.x;
        self.y += p.y;
        self
    }

    pub fn mult(&mut self, v: i32) -> &mut Self {
        self.x *= v;
        self.y *= v;
        self
    }
}

impl Sub for V2 {
    type Output = V2;

    fn sub(self, other: V2) -> V2 {
        V2 {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}

impl SubAssign for V2 {
    fn sub_assign(&mut self, _rhs: V2) {
        self.x -= _rhs.x;
        self.y -= _rhs.y;
    }
}

impl Add for V2 {
    type Output = V2;

    fn add(self, other: V2) -> V2 {
        V2 {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl Add for &V2 {
    type Output = V2;

    fn add(self, other: &V2) -> V2 {
        V2 {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl AddAssign for V2 {
    fn add_assign(&mut self, _rhs: V2) {
        self.x += _rhs.x;
        self.y += _rhs.y;
    }
}

// clockwise order
pub static MOVES: [V2; 4] = [
    V2 { x: 0, y: -1 },
    V2 { x: 1, y: 0 },
    V2 { x: 0, y: 1 },
    V2 { x: -1, y: 0 },
];

pub static MOVES0: [V2; 5] = [
    V2 { x: 0, y: 0 },
    V2 { x: 0, y: -1 },
    V2 { x: 1, y: 0 },
    V2 { x: 0, y: 1 },
    V2 { x: -1, y: 0 },
];
