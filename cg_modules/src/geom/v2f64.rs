//#![allow(dead_code)]
//#![allow(unused_variables)]
//#![allow(unused_mut)]
//#![allow(unused_imports)]

use std;
use std::convert::From;
use std::hash::{Hash, Hasher};
use std::ops::Add;
use std::ops::AddAssign;
use std::ops::Sub;
use std::ops::SubAssign;

fn main() {
    println!("{:?}", V2 { x: 1f64, y: 3f64 } + V2 { x: 2f64, y: 4f64 });
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct V2 {
    pub x: f64,
    pub y: f64,
}

impl Hash for V2 {
    fn hash<H: Hasher>(&self, state: &mut H) {
        ((self.x * 100f64) as i64).hash(state);
        ((self.y * 100f64) as i64).hash(state);
    }
}
impl Eq for V2 {}

impl V2 {
    pub fn new0() -> Self {
        V2::new(0f64, 0f64)
    }

    pub fn new(x: f64, y: f64) -> Self {
        V2 { x: x, y: y }
    }

    // fn find_centroid
    pub fn dist2(&self, p: &V2) -> f64 {
        let x = self.x - p.x;
        let y = self.y - p.y;
        x * x + y * y
    }

    pub fn dist(&self, p: &V2) -> f64 {
        self.dist2(p).sqrt()
    }

    pub fn length2(&self) -> f64 {
        (self.x * self.x + self.y * self.y)
    }

    pub fn length(&self) -> f64 {
        self.length2().sqrt()
    }

    pub fn normalize(&mut self) -> &mut Self {
        let lg = self.length();
        self.x /= lg;
        self.y /= lg;
        self
    }

    pub fn minus(&mut self, p: &V2) -> &mut Self {
        self.x -= p.x;
        self.y -= p.y;
        self
    }

    pub fn plus(&mut self, p: &V2) -> &mut Self {
        self.x += p.x;
        self.y += p.y;
        self
    }

    pub fn mult(&mut self, v: f64) -> &mut Self {
        self.x *= v;
        self.y *= v;
        self
    }

    pub fn rotate(&mut self, angle: Radian) -> &mut Self {
        let x = self.x;
        let y = self.y;
        let cos_a = angle.value.cos();
        let sin_a = angle.value.sin();
        self.x = x * cos_a - y * sin_a;
        self.y = x * sin_a + y * cos_a;
        self
    }

    pub fn to_length(&mut self, lg: f64) -> &mut Self {
        let current_lg = self.length();
        self.mult(lg / current_lg)
    }

    fn move_to(&mut self, dest: &V2, step_max: f64, near_min: f64) -> &mut Self {
        let mut mvt = dest.clone();
        mvt.minus(self);
        let lg: f64 = mvt.length();
        let coeff = ((lg - near_min) / lg).min(step_max / lg);
        mvt.mult(coeff);
        self.plus(&mvt)
    }
}

impl Sub for V2 {
    type Output = V2;

    fn sub(self, other: V2) -> V2 {
        V2 {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}

impl SubAssign for V2 {
    fn sub_assign(&mut self, _rhs: V2) {
        self.x -= _rhs.x;
        self.y -= _rhs.y;
    }
}

impl Add for V2 {
    type Output = V2;

    fn add(self, other: V2) -> V2 {
        V2 {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl AddAssign for V2 {
    fn add_assign(&mut self, _rhs: V2) {
        self.x += _rhs.x;
        self.y += _rhs.y;
    }
}

const DIRECTIONS: [V2; 8] = [
    V2 { x: 0.0, y: -1.0 },
    V2 { x: 1.0, y: -1.0 },
    V2 { x: 1.0, y: 0.0 },
    V2 { x: 1.0, y: 1.0 },
    V2 { x: 0.0, y: 1.0 },
    V2 { x: -1.0, y: 1.0 },
    V2 { x: -1.0, y: 0.0 },
    V2 { x: -1.0, y: -1.0 },
];

pub fn to_direction_i(st: String) -> i32 {
    match st.as_ref() {
        "N" => 0,
        "NE" => 1,
        "E" => 2,
        "SE" => 3,
        "S" => 4,
        "SW" => 5,
        "W" => 6,
        "NW" => 7,
        x => panic!("invalid direction {:?}", x),
    }
}

pub fn to_direction_str(i: i32) -> &'static str {
    match i {
        0 => "N",
        1 => "NE",
        2 => "E",
        3 => "SE",
        4 => "S",
        5 => "SW",
        6 => "W",
        7 => "NW",
        x => panic!("invalid direction {:?}", x),
    }
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Radian {
    pub value: f64,
}

impl From<Degree> for Radian {
    fn from(a: Degree) -> Radian {
        Radian {
            value: a.value * std::f64::consts::PI / 180.0,
        }
    }
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Degree {
    pub value: f64,
}

impl From<Radian> for Degree {
    fn from(a: Radian) -> Degree {
        Degree {
            value: a.value * 180.0 * std::f64::consts::FRAC_1_PI,
        }
    }
}
