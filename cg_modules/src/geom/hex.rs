//===============================================================================================
// see http://www.redblobgames.com/grids/hexagons/implementation.html
use std::hash::{Hash, Hasher};
use std::ops::Add;
use std::ops::AddAssign;
use std::ops::Sub;
use std::ops::SubAssign;

#[derive(Clone, Debug, Copy, PartialEq, Eq)]
pub struct Hex {
    pub q: i32,
    pub r: i32,
    pub s: i32,
}

impl Hex {
    pub fn new(q: i32, r: i32) -> Hex {
        Hex {
            q: q,
            r: r,
            s: -q - r,
        }
    }

    pub fn from_direction(d: i32) -> Hex {
        // assert!(0 <= d && d < 6);
        DIRECTIONS[((6 + (d % 6)) % 6) as usize]
    }

    pub fn from_roffset(offset: i32, col: i32, row: i32) -> Hex {
        let q = col - ((row + offset * (row & 1)) / 2);
        let r = row;
        let s = -q - r;
        Hex { q: q, r: r, s: s }
    }

    pub fn to_roffset(self, offset: i32) -> (i32, i32) {
        let col = self.q + ((self.r + offset * (self.r & 1)) / 2);
        let row = self.r;
        (col, row)
    }

    pub fn mult(self, other: i32) -> Hex {
        Hex {
            q: self.q * other,
            r: self.r * other,
            s: self.s * other,
        }
    }

    pub fn length(self) -> i32 {
        (self.q.abs() + self.r.abs() + self.s.abs()) / 2
    }

    pub fn dist(self, other: &Hex) -> i32 {
        (self - *other).length()
    }
}

impl Hash for Hex {
    fn hash<H: Hasher>(&self, state: &mut H) {
        // from http://www.redblobgames.com/grids/hexagons/implementation.html
        // let hq = hash(self.q);
        // let hr = hash(self.r);
        // (hq ^ (hr + 0x9e3779b9 + (hq << 6) + (hq >> 2))).hash(state);
        self.q.hash(state);
        self.r.hash(state);
        // no need to include s, because s could be computed from q and r
    }
}

impl Add for Hex {
    type Output = Hex;

    fn add(self, other: Hex) -> Hex {
        Hex {
            q: self.q + other.q,
            r: self.r + other.r,
            s: self.s + other.s,
        }
    }
}

impl AddAssign for Hex {
    fn add_assign(&mut self, other: Hex) {
        *self = Hex {
            q: self.q + other.q,
            r: self.r + other.r,
            s: self.s + other.s,
        };
    }
}

impl Sub for Hex {
    type Output = Hex;

    fn sub(self, other: Hex) -> Hex {
        Hex {
            q: self.q - other.q,
            r: self.r - other.r,
            s: self.s - other.s,
        }
    }
}

impl SubAssign for Hex {
    fn sub_assign(&mut self, other: Hex) {
        *self = Hex {
            q: self.q - other.q,
            r: self.r - other.r,
            s: self.s - other.s,
        };
    }
}
//------------
const DIRECTIONS: [Hex; 6] = [
    Hex { q: 1, r: 0, s: -1 },
    Hex { q: 1, r: -1, s: 0 },
    Hex { q: 0, r: -1, s: 1 },
    Hex { q: -1, r: 0, s: 1 },
    Hex { q: -1, r: 1, s: 0 },
    Hex { q: 0, r: 1, s: -1 },
];
pub fn neighbor(v: Hex, d: i32) -> Hex {
    v + Hex::from_direction(d)
}

pub trait HasLocation {
    fn location(&self) -> &Hex;
}


pub fn find_nearest<'a, T: HasLocation>(list: Vec<&'a T>, p: Hex) -> Option<&'a T> {
    if list.len() == 0 {
        return None;
    }
    let mut found = list[0];
    let mut d = p.dist(list[0].location());
    for i in 1..list.len() {
        let dd = p.dist(list[i].location());
        if dd < d {
            found = list[i];
            d = dd;
        }
    }
    Some(found)
}

pub fn find_farest<'a, T: HasLocation>(list: Vec<&'a T>, p: Hex, limit: i32) -> Option<&'a T> {
    if list.len() == 0 {
        return None;
    }
    let mut found = list[0];
    let mut d = p.dist(list[0].location());
    for i in 1..list.len() {
        let dd = p.dist(list[i].location());
        if dd > d && dd < limit {
            found = list[i];
            d = dd;
        }
    }
    Some(found)
}
