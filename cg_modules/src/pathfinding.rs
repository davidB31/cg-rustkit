use std::collections::HashMap;
use std::collections::HashSet;
use std::collections::VecDeque;
use std::hash::Hash;

pub fn scan_bfs<'a, Node: Eq + Hash + Copy, Edge: Copy, F, P>(
    src: (Node, Edge),
    find_neighbors: F,
    process: &mut P,
) where
    Node: Eq + Hash + Copy,
    Edge: Copy,
    F: Fn(&Node, &Edge) -> Vec<(Node, Edge)>,
    P: FnMut(&Node, &Edge) -> bool,
{
    let mut frontier: VecDeque<(Node, Edge)> = VecDeque::new();
    frontier.push_back(src);

    let mut came_from: HashMap<Node, (Node, Edge)> = HashMap::new();
    came_from.insert(src.0, src);
    // fill the came_from
    while !frontier.is_empty() {
        let current = frontier.pop_front().expect("frontier should not be empty");
        if process(&current.0, &current.1) {
            break;
        }
        for next in find_neighbors(&current.0, &current.1) {
            if !came_from.contains_key(&next.0) {
                frontier.push_back(next);
                came_from.insert(next.0, current);
            }
        }
    }
}

pub fn find_path_bfs<'a, Node: Eq + Hash + Copy, Edge: Copy, F, P>(
    src: (Node, Edge),
    find_neighbors: F,
    found: &mut P,
) -> Vec<(Node, Edge)>
where
    Node: Eq + Hash + Copy,
    Edge: Copy,
    F: Fn(&Node, &Edge) -> Vec<(Node, Edge)>,
    P: FnMut(&Node, &Edge) -> bool,
{
    let mut frontier: VecDeque<(Node, Edge)> = VecDeque::new();
    frontier.push_back(src);

    let mut came_from: HashMap<Node, (Node, Edge)> = HashMap::new();
    came_from.insert(src.0, src);
    let mut dst = None;
    // fill the came_from
    while !frontier.is_empty() {
        let current = frontier.pop_front().expect("frontier should not be empty");
        if found(&current.0, &current.1) {
            dst = Some(current);
            break;
        }
        for next in find_neighbors(&current.0, &current.1) {
            if !came_from.contains_key(&next.0) {
                frontier.push_back(next);
                came_from.insert(next.0, current);
            }
        }
    }
    // rebuild the path
    if let Some(dst) = dst {
        let mut current_node = dst.0;
        let mut path = vec![dst.clone()];
        while current_node != src.0 {
            let way = came_from
                .get(&current_node)
                .expect("from should be defined");
            if way.0 == src.0 {
                break;
            }
            path.push(way.clone());
            current_node = way.0;
        }
        // path.append(start) // optional
        path.reverse(); // optional
        path
    } else {
        vec![]
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    type TNode = (i32, i32);
    type TEdge = (&'static str, f32);

    #[test]
    fn test_find_path_bfs() {
        let y_max = 10;
        let y_min = 0;
        let x_max = 10;
        let x_min = 0;
        let find_neighbors = |node: &TNode, edge: &TEdge| {
            let mut b = vec![];
            // 4 directions
            for mvt in vec![
                (-1, 0, "LEFT", 1.0),
                (1, 0, "RIGHT", 1.0),
                (0, -1, "UP", 1.0),
                (0, 1, "BOTTOM", 1.0),
            ] {
                let x = node.0 + mvt.0;
                let y = node.1 + mvt.1;
                if y >= y_min && y <= y_max && x >= x_min && x <= x_max {
                    b.push(((x, y), (mvt.2, mvt.3)));
                }
            }
            b
        };
        assert_eq!(
            find_path_bfs(
                ((1, 1), ("", 0f32)),
                find_neighbors,
                &mut |n: &TNode, e: &TEdge| { n.0 == 1 && n.1 == 5 }
            ),
            vec![
                //((1, 1), ("BOTTOM", 1.0)),
                ((1, 2), ("BOTTOM", 1.0)),
                ((1, 3), ("BOTTOM", 1.0)),
                ((1, 4), ("BOTTOM", 1.0)),
                ((1, 5), ("BOTTOM", 1.0)),
            ]
        );
        assert_eq!(
            find_path_bfs(
                ((1, 1), ("", 0f32)),
                find_neighbors,
                &mut |n: &TNode, e: &TEdge| { n.0 == 5 && n.1 == 1 }
            ),
            vec![
                //((1, 1), ("RIGHT", 1.0)),
                ((2, 1), ("RIGHT", 1.0)),
                ((3, 1), ("RIGHT", 1.0)),
                ((4, 1), ("RIGHT", 1.0)),
                ((5, 1), ("RIGHT", 1.0)),
            ]
        );
        //(1, 1), (2, 1), (3, 1), (4, 1), (5, 1), (5, 2), (5, 3), (5, 4), (5, 5)
        //(1, 1), (2, 1), (2, 2), (3, 2), (3, 3), (4, 3), (4, 4), (5, 4), (5, 5)
        assert_eq!(
            find_path_bfs(
                ((1, 1), ("", 0f32)),
                find_neighbors,
                &mut |n: &TNode, e: &TEdge| { n.0 == 5 && n.1 == 5 }
            ),
            vec![
                //((1, 1), ("", 1.0)),
                ((2, 1), ("RIGHT", 1.0)),
                ((3, 1), ("RIGHT", 1.0)),
                ((4, 1), ("RIGHT", 1.0)),
                ((5, 1), ("RIGHT", 1.0)),
                ((5, 2), ("BOTTOM", 1.0)),
                ((5, 3), ("BOTTOM", 1.0)),
                ((5, 4), ("BOTTOM", 1.0)),
                ((5, 5), ("BOTTOM", 1.0)),
            ]
        );
    }
}

// TODO check with  https://www.codingame.com/direct-puzzle/path-finding
