use rand::Rng;
use std::fmt::Debug;

pub fn exchange<R, G>(genome: &mut [G], rng: &mut R, mutation_rate: f64)
where
    R: Rng,
    G: Sized,
{
    if rng.next_f64() > mutation_rate {
        return;
    }
    let len = genome.len();
    let g1 = rng.gen_range(0, len - 2);
    let g2 = rng.gen_range(g1 + 1, len - 1);
    genome.swap(g1, g2)
}

pub fn scramble<R, G>(genome: &mut [G], rng: &mut R, mutation_rate: f64, min_span_size: usize)
where
    R: Rng,
    G: Sized,
{
    if rng.next_f64() > mutation_rate {
        return;
    }
    let (begin, end) = choose_selection(rng, genome.len(), min_span_size);
    let mut nb_swap = end - begin;
    let range = end - begin + 1;
    while nb_swap > 0 {
        // avoid to have the same value to g1 & g2
        // TODO bench range with module vs loop until different
        let g1 = rng.gen_range(0, range - 1);
        let g2 = (g1 + rng.gen_range(1, range - 1)) % range;
        print_debug!("swaps --> {:?}, {:?}", begin + g1, begin + g2);
        genome.swap(begin + g1, begin + g2);
        nb_swap -= 1;
    }
}

fn choose_selection<R: Rng>(rng: &mut R, len: usize, min_span_size: usize) -> (usize, usize) {
    let begin = rng.gen_range(0, len - 1 - min_span_size);
    let end = rng.gen_range(begin + min_span_size, len - 1);
    //assert!((end - begin) >= min_span_size);
    (begin, end)
}

// IM insertion mutation, displace only one gene
pub fn displace_one<R, G>(genome: &mut Vec<G>, rng: &mut R, mutation_rate: f64, start_pos: usize)
where
    R: Rng,
    G: Sized + Clone,
{
    let len = genome.len();
    if rng.next_f64() > mutation_rate || start_pos > len - 2 {
        return;
    }
    let range = len - start_pos;
    // avoid to have the same value to g1 & g2
    // TODO bench range with module vs loop until different
    let g1 = rng.gen_range(0, range - 1);
    let g2 = (g1 + rng.gen_range(1, range - 1)) % range;
    //let v = genome[g1].clone();
    // TODO Optimize to avoid shift of every element after g1 and g2
    let v = genome.remove(start_pos + g1);
    genome.insert(start_pos + g2, v);
}

#[cfg(test)]
mod tests {
    use super::*;
    use rand;
    use spectral::prelude::*;

    #[test]
    fn test_choose_selection() {
        let mut rng = rand::thread_rng();
        for i in 0..100 {
            let min_span_size = 3;
            let len = 8;
            let (begin, end) = choose_selection(&mut rng, len, min_span_size);
            assert_that(&begin).is_less_than_or_equal_to(len - 1 - min_span_size);
            assert_that(&end).is_less_than_or_equal_to(len - 1);
            assert_that(&(end - begin)).is_greater_than_or_equal_to(&min_span_size)
        }
    }

    #[test]
    fn test_exchange() {
        let mut genome = vec![0, 1, 2, 3, 4, 5, 6, 7];
        let mut rng = rand::thread_rng();
        exchange(&mut genome, &mut rng, 1.0);
        // print_debug!("genome --> {:?}", genome);
        let mut change_cnt = 0;
        for i in 0..8 {
            assert_that(&genome[i]).is_greater_than_or_equal_to(&0);
            assert_that(&genome[i]).is_less_than_or_equal_to(&7);
            change_cnt += if genome[i] != i as i32 { 1 } else { 0 };
        }
        assert_that(&change_cnt).is_equal_to(&2);
    }

    #[test]
    fn test_displace_one() {
        let mut genome = vec![0, 1, 2, 3, 4, 5, 6, 7];
        let mut rng = rand::thread_rng();
        let start_pos = 2;
        displace_one(&mut genome, &mut rng, 1.0, start_pos);
        // print_debug!("genome --> {:?}", genome);
        let mut change_cnt = 0;
        for i in 0..start_pos {
            assert_that(&genome[i]).is_equal_to(&i);
        }
        for i in start_pos..genome.len() {
            assert_that(&genome[i]).is_greater_than_or_equal_to(&0);
            assert_that(&genome[i]).is_less_than_or_equal_to(&7);
            change_cnt += if genome[i] != i { 1 } else { 0 };
        }
        assert_that(&change_cnt).is_greater_than_or_equal_to(&1);
    }

    #[test]
    fn test_scramble() {
        let mut genome = vec![0, 1, 2, 3, 4, 5, 6, 7];
        let mut rng = rand::thread_rng();
        scramble(&mut genome, &mut rng, 1.0, 3);
        // print_debug!("genome --> {:?}", genome);
        let mut change_cnt = 0;
        for i in 0..8 {
            assert_that(&genome[i]).is_greater_than_or_equal_to(&0);
            assert_that(&genome[i]).is_less_than_or_equal_to(&7);
            change_cnt += if genome[i] != i as i32 { 1 } else { 0 };
        }
        assert_that(&change_cnt).is_greater_than(&0);
    }
}
