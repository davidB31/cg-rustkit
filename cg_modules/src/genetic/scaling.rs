use rand::Rng;

pub struct Fitnesses {
    pub values: Vec<f64>,
    pub sum: f64,
}

/// Rank Scaling, convert the rank (reverse) as fitnesses
pub fn rank<R>(rng: &mut R, fitnesses_desc: &[f64]) -> Fitnesses {
    let mut values = (1..fitnesses_desc.len() + 1)
        .map(|i| i as f64)
        .collect::<Vec<_>>();
    values.reverse();
    Fitnesses {
        values,
        sum: ((fitnesses_desc.len() + 2) * fitnesses_desc.len()) as f64 / 2_f64,
    }
}
// void CgaTSP::FitnessScaleBoltzmann(vector<SGenome> &pop) {
// //reduce the temp a little each generation m_dBoltzmannTemp -= BOLTZMANN_DT;
// //make sure it doesn't fall below minimum value if (m_dBoltzmannTemp<
// BOLTZMANN_MIN_TEMP) {
// m_dBoltzmannTemp = BOLTZMANN_MIN_TEMP; }
// //first calculate the average fitness/Temp
// double divider = m_dAverageFitness/m_dBoltzmannTemp;
// //now iterate through the population and calculate the new expected //values
// for (int gen=0; gen<pop.size(); ++gen)
// {
// double OldFitness = pop[gen].dFitness;
// pop[gen].dFitness = (OldFitness/m_dBoltzmannTemp)/divider; }
// //recalculate values used in selection
// CalculateBestWorstAvTot(); }
// In the TSP solver, the temperature is initially set to twice the number of
// cities. BOLTZMANN_DT is #defined as 0.05 and BOLTZMANN_MIN_TEMP is #defined
// as 1.
