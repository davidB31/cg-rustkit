use rand::Rng;

pub fn roulette_wheel<R>(rng: &mut R, fitness_total: f64, fitnesses: &[f64]) -> usize
where
    R: Rng,
{
    // rouletteWheel
    let v = rng.next_f64() * fitness_total;
    let mut cursor = 0_f64;
    for i in 0..fitnesses.len() {
        cursor += fitnesses[i];
        if cursor >= v {
            return i;
        }
    }
    0
}
