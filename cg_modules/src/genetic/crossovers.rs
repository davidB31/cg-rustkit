// permutation-encoded chromosome:
// * Partially-Mapped Crossover,
// * Order Crossover,
// * Alternating-Position Crossover,
// * Maximal-Preservation Crossover,
// * Position-Based Crossover,
// * Edge- Recombination Crossover,
// * Subtour-Chunks Crossover,
// * Intersection Crossover

use rand::Rng;
use std::fmt::Debug;
use std::ptr;
//use std::borrow::Cow;

// PMX aka Partially-Mapped Crossover
// Cow or clone ?
pub fn partially_mapped_x<R, G>(
    parent1: &[G],
    parent2: &[G],
    rng: &mut R,
    crossover_rate: f64,
) -> [Vec<G>; 2]
where
    G: Sized + Clone + PartialEq,
    R: Rng,
{
    let mut child1 = parent1.to_vec();
    let mut child2 = parent2.to_vec();
    if ptr::eq(parent1, parent2) || rng.next_f64() > crossover_rate {
        return [child1, child2];
    }
    let len = parent1.len(); //TODO avoid to select all (aka 0..len -1)
    let g1 = rng.gen_range(0, len - 1);
    let g2 = (g1 + rng.gen_range(1, len - 1)) % len;

    let begin = g1.min(g2);
    let end = g1.max(g2);
    (begin..end).for_each(|i| {
        let gen1 = &parent1[i];
        let gen2 = &parent2[i];
        if gen1 != gen2 {
            // if gen are costly to clone, maybe replace by find index and then swap
            for elem in child1.iter_mut() {
                if elem == gen1 {
                    *elem = gen2.clone();
                } else if elem == gen2 {
                    *elem = gen1.clone();
                }
            }
            for elem in child2.iter_mut() {
                if elem == gen1 {
                    *elem = gen2.clone();
                } else if elem == gen2 {
                    *elem = gen1.clone();
                }
            }
        }
    });
    //unimplemented!()
    [child1, child2]
}

#[cfg(test)]
mod tests {
    use super::*;
    use rand;
    use spectral::prelude::*;
    use std::fmt::Debug;

    #[test]
    fn partially_mapped_x_should_generate_different_children() {
        //for _ in 0..100 {
        let parent1 = vec![0, 1, 2, 3, 4, 5, 6, 7];
        let mut parent2 = vec![0, 1, 2, 3, 4, 5, 6, 7];
        parent2.reverse();
        let mut rng = rand::thread_rng();
        let children = partially_mapped_x(&parent1, &parent2, &mut rng, 1.0_f64);
        assert_that(&children[0]).is_not_equal_to(&children[1]);
        assert_that(&children[0]).is_not_equal_to(&parent1);
        assert_that(&children[0]).is_not_equal_to(&parent2);
        assert_that(&children[1]).is_not_equal_to(&parent1);
        assert_that(&children[1]).is_not_equal_to(&parent2);
        // TODO check that the mapped was done
        //}
    }
}
