use std::fmt;
use std::mem;
use std::ptr;

const H_MAX: usize = 7;
const W_MAX: usize = 7;
pub type Item = u32;

macro_rules! fori {
    ($i:ident, $i0:expr, $i1:expr, $code:stmt) => {{
        let mut $i = $i0;
        while $i < $i1 {
            $code;
            $i += 1;
        }
    }};
}

pub struct Grid<Item> {
    data: [Item; (W_MAX * H_MAX) as usize],
    width: usize,
    height: usize,
}

impl Grid<Item> {
    pub fn new(w: usize, h: usize) -> Self {
        assert!(w <= W_MAX);
        assert!(h <= H_MAX);
        Grid {
            data: [0; W_MAX * H_MAX],
            width: w,
            height: h,
        }
    }
    pub fn get(&self, x: usize, y: usize) -> Item {
        self.data[x + y * W_MAX]
        // unsafe { *self.data.get_unchecked(x + y * W) }
    }

    pub fn set(&mut self, x: usize, y: usize, v: Item) -> bool {
        if y < H_MAX {
            self.data[x + y * W_MAX] = v;
            true
        } else {
            false
        }
    }

    fn copy_from(&mut self, src: &Grid<Item>) {
        unsafe {
            ptr::copy_nonoverlapping(src.data.as_ptr(), self.data.as_mut_ptr(), W_MAX * H_MAX);
        }
        self.width = src.width;
        self.height = src.height
    }
}

impl Clone for Grid<Item> {
    fn clone(&self) -> Self {
        let mut dst = Grid::new(self.width, self.height);
        dst.copy_from(self);
        dst
    }
}

impl fmt::Debug for Grid<Item> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "-----\n").ok();
        fori!(y, 0, self.height, {
            fori!(x, 0, self.width, {
                write!(f, "{} ", self.get(x, y)).ok();
            });
            write!(f, "\n").ok();
        });
        write!(f, "-----\n")
    }
}

impl PartialEq for Grid<Item> {
    //TODO optimize
    fn eq(&self, other: &Grid<Item>) -> bool {
        let mut r = self.width == other.width && self.height == other.height;
        if r {
            fori!(y, 0, self.height, {
                fori!(x, 0, self.width, { r = self.get(x, y) == other.get(x, y) });
            });
        }
        r
    }
}
