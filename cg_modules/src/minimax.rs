/// try to implement minimax + alpha-beta pruning + eval (or timer restriction)
use std::i32;

pub trait GraphExplorer<Node, Edge>
    where Edge: Copy
{
    fn edges(&self, n: &Node) -> Vec<Edge>;
    fn transition(&self, n: &Node, edge: &Edge) -> Node;
    fn utility(&self, n: &Node) -> i32;
}

pub fn minimax<Node, Edge>(n0: &Node, graph: &GraphExplorer<Node, Edge>) -> (Option<Edge>, i32)
    where Edge: Copy
{
    run_max(n0, graph, i32::MIN, i32::MAX)
}

fn run_max<Node, Edge>(n: &Node,
                       graph: &GraphExplorer<Node, Edge>,
                       alpha0: i32,
                       beta0: i32)
                       -> (Option<Edge>, i32)
    where Edge: Copy
{
    let edges = graph.edges(n);
    if edges.is_empty() {
        return (None, graph.utility(n));
    }
    let mut alpha = alpha0;
    let mut beta = beta0;
    let mut result_a = &edges[0];
    let mut result_u = i32::MIN;
    for a in edges.iter() {
        let child_n = graph.transition(n, &a);
        let (_, child_u) = run_min(&child_n, graph, alpha, beta);
        if child_u > result_u {
            result_a = a;
            result_u = child_u;
        }
        if result_u >= beta {
            return (Some(*result_a), result_u);
        }
        if result_u > alpha {
            alpha = result_u
        }
    }
    return (Some(*result_a), result_u);
}
//TODO
fn run_min<Node, Edge>(n: &Node,
                       graph: &GraphExplorer<Node, Edge>,
                       alpha0: i32,
                       beta0: i32)
                       -> (Option<Edge>, i32)
    where Edge: Copy
{
    let edges = graph.edges(n);
    if edges.is_empty() {
        return (None, graph.utility(n));
    }
    let mut alpha = alpha0;
    let mut beta = beta0;
    let mut result_a = &edges[0];
    let mut result_u = i32::MAX;
    for a in edges.iter() {
        let child_n = graph.transition(n, &a);
        let (_, child_u) = run_max(&child_n, graph, alpha, beta);
        if child_u < result_u {
            result_a = a;
            result_u = child_u;
        }
        if result_u <= alpha {
            return (Some(*result_a), result_u);
        }
        if result_u < beta {
            beta = result_u
        }
    }
    return (Some(*result_a), result_u);
}

#[cfg(test)]
mod tests {
    use super::*;
    // a binary tree of 5 level
    // node value = 0 => has child
    // node value = -1 =>  doesn't exist
    struct MyGraph {
        nodes: [i32; 31],
        utility_delta: i32,
    }

    impl GraphExplorer<i32, i32> for MyGraph {
        fn edges(&self, n: &i32) -> Vec<i32> {
            let id = *n as usize;
            let u = self.nodes[id];
            let mut edges: Vec<i32> = vec![];
            if id > 31 || u != 0 {
                // nothing to add
            } else {
                if self.nodes[id * 2 + 1] > -1 {
                    edges.push(0)
                }
                if self.nodes[id * 2 + 2] > -1 {
                    edges.push(1)
                }
            }
            edges
        }
        fn transition(&self, n: &i32, edge: &i32) -> i32 {
            *n * 2 + 1 + *edge
        }
        fn utility(&self, n: &i32) -> i32 {
            self.nodes[*n as usize] + &self.utility_delta
        }
    }

    #[test]
    fn test_minimax_basic() {
        let n0 = 0;
        let my_graph = MyGraph {
            utility_delta: 0,
            nodes: [
                0, //level 0
                0,0, // level 1
                0,0,0,0, // level 2
                0,0,0,0,3,7,0,0, //level 3
                6,11,4,2,14,-1,9,4,-1,-1,-1,-1,9,-1,12,20, //level 4
            ],
        };
        let (edge, u) = minimax(&n0, &my_graph);
        assert_eq!(Some(1), edge);
        assert_eq!(7, u);
    }
}
