#[macro_export]
macro_rules! print_debug {
    ($($arg:tt)*) => (
        {
            eprintln!($($arg)*);
        }
    )
}
