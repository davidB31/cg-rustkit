#![allow(dead_code)]
#![allow(unused_variables)]
#![allow(unused_mut)]
#![allow(unused_imports)]

extern crate rand;

#[macro_use]
#[cfg(test)]
extern crate spectral;

#[macro_use]
mod macros;

pub mod geom;
//pub mod geom_v2i;
//pub mod geom::hex;
pub mod genetic;
pub mod grid;
pub mod minimax;
pub mod pathfinding;
