extern crate cg_modules;
extern crate rand;
#[cfg(test)]
#[macro_use]
extern crate spectral;

use cg_modules::genetic;
use cg_modules::genetic::GenotypeCtrl;
use cg_modules::geom::v2::V2;
//use rand;
use rand::Rng;
use spectral::prelude::*;
//use std::cmp::Ordering;
//use std::ops::IndexMut;

struct Cities {
    coords: Vec<V2>,
    /* area_max: V2, */
    /* area_min: V2, */
}

impl Cities {
    fn new_circular(nb_cities: usize, radius: f64) -> Cities {
        let angle_interval = std::f64::consts::PI * 2.0 / (nb_cities as f64);
        let coords = (0..nb_cities)
            .map(|i| {
                let a = i as f64 * angle_interval;
                V2::new(a.cos() * radius, a.sin() * radius)
            })
            .collect::<Vec<_>>();
        Cities { coords }
    }
}

fn distance_loop(path: &Vec<V2>) -> f64 {
    path.iter()
        .fold((0f64, None), |acc, v| match acc.1 {
            None => (v.dist(path.last().unwrap()), Some(v)),
            Some(previous) => (acc.0 + v.dist(&previous), Some(v)),
        })
        .0
}

#[derive(Clone, Debug)]
struct Solution {
    city_ids: Vec<usize>,
    fitness: f64,
}

struct SolutionCtrlCfg {
    mutation_rate: f64,
    crossover_rate: f64,
    population_size: usize,
    elitism: usize,
}

struct SolutionCtrl<'a> {
    rng: rand::ThreadRng,
    cfg: SolutionCtrlCfg,
    fitness_total: f64,
    solutions: Vec<Solution>,
    generation_current: u32,

    cities: &'a Cities,
    tour_shortest: f64,
    tour_longest: f64,
}

impl<'a> GenotypeCtrl<Solution> for SolutionCtrl<'a> {
    fn generate(&mut self) -> Solution {
        // could be optimized
        //let limit = self.cities.coords.len() - 1;
        let mut city_ids = (0..self.cities.coords.len()).collect::<Vec<_>>();
        // .fold(vec![] as Vec<usize>, |mut acc, _| {
        //     let mut v = self.rng.gen_range(0, limit);
        //     while acc.contains(&v) {
        //         v = self.rng.gen_range(0, limit);
        //     }
        //     acc.push(v);
        //     acc
        // });
        self.rng.shuffle(&mut city_ids);
        Solution {
            city_ids,
            fitness: 0f64,
        }
    }

    fn merge(&mut self, g1: &Solution, _g2: &Solution) -> Solution {
        Solution {
            city_ids: g1.city_ids.clone(),
            fitness: 0f64,
        }
    }

    fn mutate(&mut self, g1: &Solution) -> Solution {
        Solution {
            city_ids: g1.city_ids.clone(),
            fitness: 0f64,
        }
    }

    fn evaluate(&mut self, g1: &Solution) -> f64 {
        let path = g1.city_ids
            .iter()
            .map(|i| self.cities.coords[*i])
            .collect::<Vec<_>>();
        1000f64 / distance_loop(&path)
    }

    fn shuffle(&mut self, _l: &mut [Solution]) {}
}

impl<'a> SolutionCtrl<'a> {
    fn new(cfg: SolutionCtrlCfg, cities: &'a Cities) -> Self {
        SolutionCtrl {
            rng: rand::thread_rng(),
            cfg,
            fitness_total: 0f64,
            generation_current: 0,
            solutions: vec![],

            cities,
            tour_shortest: 0f64,
            tour_longest: 0f64,
        }
    }
    fn calculate_populations_fitness(&mut self) {
        fn estimate(g: &Solution, cities: &Cities) -> f64 {
            let path = g.city_ids
                .iter()
                .map(|i| cities.coords[*i])
                .collect::<Vec<_>>();
            distance_loop(&path)
        }
        self.tour_shortest = std::f64::MAX;
        self.tour_longest = std::f64::MIN;
        for s in self.solutions.iter_mut() {
            let fitness = estimate(s, self.cities);
            s.fitness = fitness;
            self.tour_shortest = self.tour_shortest.min(fitness);
            self.tour_longest = self.tour_longest.max(fitness);
        }
        self.fitness_total = 0_f64;
        for s in self.solutions.iter_mut() {
            s.fitness = self.tour_longest - s.fitness;
            self.fitness_total += s.fitness;
        }
    }

    fn epoch(&mut self) {
        //self.reset();
        self.calculate_populations_fitness();
        // if self.tour_shortest <= cities.best {
        //     return;
        // }
        let mut population_next: Vec<Solution> = Vec::with_capacity(self.cfg.population_size);
        // elitism
        self.solutions
            .sort_by(|a, b| b.fitness.partial_cmp(&a.fitness).expect("defined f64"));
        self.solutions
            .iter()
            .take(self.cfg.elitism)
            .cloned()
            .for_each(|s| population_next.push(s));
        let fitnesses_orig = self.solutions.iter().map(|s| s.fitness).collect::<Vec<_>>();
        let rng = &mut self.rng;
        let mutation_rate = self.cfg.mutation_rate;
        let crossover_rate = self.cfg.crossover_rate;
        let fitnesses = genetic::scaling::rank(rng, &fitnesses_orig);
        while population_next.len() < self.cfg.population_size {
            //TODO optimize find a way to return a ref to Genome (without clone)
            let mum = &self.solutions
                [genetic::selections::roulette_wheel(rng, fitnesses.sum, &fitnesses.values)];
            let dad = &self.solutions
                [genetic::selections::roulette_wheel(rng, fitnesses.sum, &fitnesses.values)];
            genetic::crossovers::partially_mapped_x(
                &mum.city_ids,
                &dad.city_ids,
                rng,
                crossover_rate,
            ).iter_mut()
                .map(|mut child| {
                    genetic::mutations::exchange(&mut child, rng, mutation_rate);
                    Solution {
                        city_ids: child.to_vec(), //TODO remove the copy
                        fitness: 0_f64,
                    }
                })
                .for_each(|s| population_next.push(s));
        }
        self.solutions = population_next;
        self.generation_current += 1;
    }
    // reset the variables (?)
    // fn reset(&mut self) {
    //     //unimplemented!()
    // }

    fn create_starting_population(&mut self) {
        self.solutions.clear();
        (0..self.cfg.population_size).for_each(|_| {
            let solution = self.generate();
            self.solutions.push(solution);
        });
    }
}
// impl Genome<usize> for Vec<usize> {
//     // fn len(&self) -> usize {
//     //     self.len()
//     // }
// }

#[test]
fn create_cities() {
    let cities = Cities::new_circular(4, 1.0f64);
    assert_that!(&cities.coords.len()).is_equal_to(&4);
    assert_that!(&distance_loop(&cities.coords)).is_close_to(4f64 * 2f64.sqrt(), 0.0001f64);
}

#[test]
fn single_run() {
    let cities = Cities::new_circular(20, 200_f64);
    let mut solution_ctrl = SolutionCtrl::new(
        SolutionCtrlCfg {
            mutation_rate: 0.2_f64,
            crossover_rate: 0.75_f64,
            population_size: 40,
            elitism: 1,
        },
        &cities,
    );
    solution_ctrl.create_starting_population();
    println!("len {:?}", solution_ctrl.solutions.len());
    for _ in 0..200 {
        solution_ctrl.epoch();
        println!(
            "{:?} // {:?} - {:?}",
            solution_ctrl.solutions[0], solution_ctrl.tour_longest, solution_ctrl.tour_shortest
        );
    }
    //used to rectify precision errors #define EPSILON 0.000001
    //#define WINDOW_WIDTH 500
    //#define WINDOW_HEIGHT 500
    //#define NUM_CITIES 20 #define CITY_SIZE 5
    //#define MUTATION_RATE 0.2 #define CROSSOVER_RATE 0.75 #define POP_SIZE 40
    //must be a multiple of 2 #define NUM_BEST_TO_ADD 2
}
